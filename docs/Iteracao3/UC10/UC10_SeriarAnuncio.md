# UC 10 - Seriar (Não Automaticamente) Candidaturas de Anúncio

## 1. Engenharia de Requisitos

### Formato Breve

O colaborador de organização inicia o processo não automático de seriação dos candidatos à realização de um anúncio por si publicado. O sistema solicita dados (i.e. o anúncio, a classificação de cada uma das candidaturas e os outros colaboradores da organização participantes no processo). O colaborador introduz os dados solicitados. O sistema valida e apresenta os dados, pedindo que os confirme. O colaborador confirma. O sistema regista os dados juntamente com a data/hora atual, verifica a obrigatoriedade da atribuição, prossegue com o processo de atribuição e regista o mesmo e informa o colaborador do sucesso da operação.

### SSD
![UC10-SSD.scg](UC10_SSD.svg)


### Formato Completo

#### Ator principal

* Colaborador de Organização

#### Partes interessadas e seus interesses

* **Colaborador de Organização:** pretende seriar as candidaturas que um anúncio recebeu.
* **Freelancer:** pretende conhecer a classificação das suas candidaturas à realização de determinados anúncios publicados na plataforma.
* **Organização:** pretende contratar pessoas externas (outsourcing) para a realização de determinadas tarefas e com competências técnicas apropriadas.
* **T4J:** pretende satisfazer as organizações e os freelancer facilitando a contratação de freelancers pelas organizações e vice-versa.

#### Pré-condições

* Existir pelo menos um anúncio de tarefa em condições de ser seriado manualmente pelo colaborador ativo no sistema.

#### Pós-condições
* A informação do processo de seriação é registada no sistema.
* Caso o anúncio possua um tipo de regimento com atribuição obrigatória, registar o processe de atribuição e respetiva adjudicação.

#### Cenário de sucesso principal (ou fluxo básico)

1. O colaborador inicia o processo não automático de seriação das candidaturas a um anúncio.
2. O sistema mostra os anúncios publicadas pelo colaborador em fase de seriação não automática e que ainda não foram seriados e pede-lhe para escolher um.
3. O colaborador seleciona um anúncio.
4. O sistema mostra as candidaturas que o anúncio selecionado recebeu e que ainda estejam por classificar e solicita a escolha de uma dessas candidaturas.
5. O colaborador seleciona uma candidatura.
6. O sistema solicita a classificação da candidatura selecionada e uma justificação de suporte à classificação.
7. O colaborador introduz a classificação e a justificação de suporte à classificação.
8. Os passos 4 a 7 repetem-se até que estejam classificadas todas as candidaturas.
9. O sistema mostra a lista dos outros colaboradores da mesma organização (não selecionado) e solicita a seleção de um colaborador participante no processo de seriação.
10. O colaborador seleciona um colaborador.
11. Os passos 9 e 10 repetem-se até que estejam selecionados todos os outros colaboradores participantes no processo de seriação.
12. O sistema solicita a introdução de um texto de resumo do processo de seriação.
13. O colaborador introduz.
14. O sistema valida e apresenta os dados, pedindo ao colaborador para confirmar.
15. O colaborador confirma.
16. O sistema regista os dados juntamente com a data/hora atual, verifica que a atribuição do anúncio é obrigatória e inicia o processo de atribuição. O sistema obtém os dados necessários para a adjudicacao da tarefa(org, freelancer, dsTarefa, dataInicioAtribuicao, dataFimAtribuicao, valorAceite, idAnuncio, numSequencial, dataAdjudicacao). O sistema regista o processo de atribuição e informa o utilizador do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O colaborador solicita o cancelamento do processo de seriação das candidaturas.
> O caso de uso termina.

14a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 2)
>
	> 2a. O colaborador não altera os dados. O caso de uso termina.

16a. A atribuição do anúncio é opcional
> 1. O sistema não inicia o processo de atribuição.
	> O caso de uso termina.



#### Requisitos especiais

\-

#### Lista de Variações de Tecnologias e Dados

\-

#### Frequência de Ocorrência

\-

#### Questões em aberto

* O mesmo lugar/classificação pode ser atribuído a mais do que uma candidatura (e.g. em caso de empate)?
* O processo de seriação pode ser concluído havendo candidaturas por classificar?
* Há algum motivo que possa levar à desclassificação de uma candidatura?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC10-MD.scg](UC10_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
|1. O colaborador inicia o processo não automático de seriação das candidaturas a um anúncio.|... interage com o colaborador de organização?| SeriarAnuncioUI |Pure Fabrication|
| |... coordena o UC?| SeriarAnuncioController |Controller|
| |...conhece o utilizador/gestor a usar o sistema?|SessaoUtilizador|IE: cf. documentação do componente de gestão de utilizadores.|
| |...conhece o colaborador em questão?|ListaColaboradores|Pela aplicação do IE as instâncias de Colaborador seriam conhecidas pela Organizacao. Mas, por aplicação de HC+LC à Organizacao, esta delega essa responsabilidade à ListaColaboradores.|
| |...conhece a organização a que o colaborador pertence?|RegistoOrganizacoes|Pela aplicação do IE as instâncias da Organizacao seriam conhecidas pela Plataforma. Mas, por aplicação de HC+LC à Plataforma, esta delega essa responsabilidade à RegistoOrganizacoes.|
| |...conhece RegistoOrganizacoes?|Plataforma|IE: Plataforma possui RegistoOrganizacoes|
| |...cria instância de ProcessoSeriacao?|Anuncio|Creator (Regra1): no MD a Anuncio possui ProcessoSeriacao|
|2. O sistema mostra os anúncios publicadas pelo colaborador em fase de seriação não automática e que ainda não foram seriados e pede-lhe para escolher um.|...conhece a lista de anúncios?|RegistoAnuncios|Pela aplicação do IE as instâncias da Anuncio seriam conhecidas pela Plataforma. Mas, por aplicação de HC+LC à Plataforma, esta delega essa responsabilidade à RegistoAnuncios.|
| |...conhece RegistoAnuncios|Plataforma|IE: Plataforma possui a instância de RegistoAnuncios|
|3. O colaborador seleciona um anúncio.||||
|4. O sistema mostra as candidaturas que o anúncio selecionado recebeu e que ainda estejam por classificar e solicita a escolha de uma dessas candidaturas.|...conhece a lista de candidaturas?|ListaCandidaturas|Pela aplicação do IE as instâncias de Candidatura seriam conhecidas pelo Anuncio. Mas, por aplicação de HC+LC ao Anuncio, esta delega essa responsabilidade à ListaCandidaturas.|
| |...conhece ListaCandidaturas|Anuncio|IE: no MD Anuncio possui a instância ListaCandidaturas|
|5. O colaborador seleciona uma candidatura.||||
|6. O sistema solicita a classificação da candidatura selecionada e uma justificação de suporte à classificação.|
|7. O colaborador introduz a classificação e a justificação de suporte à classificação.|...guarda os dados solicitados?|Classificacao|IE: possui os seus próprios dados.|
| |...valida a Classificacao?(validação local)|Classificacao|IE: possui os seus próprios dados|
| |...guarda a Classificacao criada?|ProcessoSeriacao|IE: no MD ProcessoSeriacao tem Classificacao|
|8. Os passos 4 a 7 repetem-se até que estejam classificadas todas as candidaturas.||||
|9. O sistema mostra a lista dos outros colaboradores da mesma organização (não selecionado) e solicita a seleção de um colaborador participante no processo de seriação.|...conhece a lista de colaboradores?|ListaColaboradores|HC + LC|
|10. O colaborador seleciona um colaborador.|...guarda o colaborador selecionado?|ProcessoSeriacao|ProcessoSeriacao tem os seus próprios dados|
|11. Os passos 9 e 10 repetem-se até que estejam selecionados todos os outros colaboradores participantes no processo de seriação.||||
|12. O sistema solicita a introdução de um texto de resumo do processo de seriação.|
|13. O colaborador introduz.|...guarda o dado solicitado?|ProcessoSeriacao|ProcessoSeriacao tem os seus próprios dados.|
|14. O sistema valida e apresenta os dados, pedindo ao colaborador para confirmar.|...valida os dados de ProcessoSeriacao?(validação local)|ProcessoSeriacao|IE: possui os seus próprios dados|
|15. O colaborador confirma.||||
|16. O sistema regista os dados juntamente com a data/hora atual, verifica que a atribuição do anúncio é obrigatória e inicia o processo de atribuição. O sistema obtém os dados necessários para a adjudicacao da tarefa(org, freelancer, dsTarefa, dataInicioTarefa, nrDiasNecessarios, valorAceite, anuncio, numSequencial, dataAdjudicacao). O sistema regista o processo de atribuição e informa o utilizador do sucesso da operação.|guarda o ProcessoSeriacao criado?|Anuncio|IE: no MD Anuncio tem ProcessoSeriacao|
| |...cria a instância de ProcessoAtribuicao|Anuncio|Creator: Anuncio espoleta ProcessoAtribuicao|
| |...cria a instância de Adjudicaaco|ProcessoAtribuicao|Creator: ProcessoAtribuicao resulta em Adjudicaacao|
| |...verifica a obrigatoriedade do anúncio?|TipoRegimento|TipoRegimento tem os seus próprios dados|


### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Anuncio
 * Candidatura
 * ProcessoSeriacao
 * TipoRegimento
 * Classificacao
 * Organizacao
 * Colaborador

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * SeriarAnuncioUI  
 * SeriarAnuncioController
 * RegistoAnuncios
 * RegistoOrganizacoes
 * ListaColaboradores
 * ListaCandidaturas


###	Diagrama de Sequência

![UC10_SD.svg](UC10_SD.svg)

##### UC10_SD_GetAnunciosSeriaveisNaoAutomaticos

![UC10_SD_GetAnunciosSeriaveisNaoAutomaticos.svg](UC10_SD_GetAnunciosSeriaveisNaoAutomaticos.svg)

##### UC10_SD_AtribuirTarefa

![UC10_SD_AtribuirTarefa.svg](UC10_SD_AtribuirTarefa.svg)

###	Diagrama de Classes

![UC10_CD.svg](UC10_CD.svg)

**Nota:** Algumas dependências estão omitidas.
