# UC12 - Retirar candidatura submetida

## 1. Engenharia de Requisitos

### Formato Breve

O freelancer inicia a atualização de uma candidatura submetida. O sistema solicita a candidatura a ser atualizada. O freelancer seleciona uma candidatura. O sistema apresenta os dados da candidatura e permite a sua alteração. O freelancer altera os dados desejados. O sistema apresenta os dados e solicita confirmação. O freelancer confirma os dados. O sistema regista os dados, atualiza a candidatura e informa o freelancer do sucesso da operação.

### SSD
![UC11_SSD.svg](UC11_SSD.svg)


### Formato Completo

#### Ator principal

Freelancer

#### Partes interessadas e seus interesses
* **Freelancer** pretende que haja a possibilidade de retirar uma candidatura previamente submetida.
* **T4J:** pretende que os freelancer usufruam de uma maior flexibilidade, podendo retirar as candidaturas previamente submetidas.


#### Pré-condições
* A candidatura que o freelancer pretende atualizar deve estar registada no sistema.

* A operação de atualizar a candidatura submetida só deverá estar disponível no período de candidaturas do anúncio ao qual o freelancer se candidatou.

* O freelancer só poderá atualizar as candidaturas que ele efetuou.


#### Pós-condições
* A candidatura escolhida pelo freelancer é atualizada.

### Cenário de sucesso principal (ou fluxo básico)

1. O freelancer inicia a atualização de uma candidatura submetida.
2. O sistema apresenta ao freelancer a sua lista de candidaturas e solicita a seleção de uma.
3. O freelancer seleciona uma candidatura.
4. O sistema solicita os dados a alterar de modo a atualizar a candidatura(i.e. valor pretendido, numero dias, texto apresentacão (opcional), texto motivação (opcional)).
5. O freelancer escolhe os dados que pretende alterar e atualiza-os.
6. O sistema valida, apresenta os dados atualizados e pede a sua confirmação.
7. O freelancer atualiza os dados.
8. O sistema atualiza a candidatura e informa o freelancer do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O freelancer solicita o cancelamento da atualização da candidatura submetida.

> O caso de uso termina.

4a. O sistema deteta que o anuncio para qual o freelancer se candidatou já não se encontra no período de candidaturas.
>	1. O sistema alerta o freelancer para o facto.

> 2. O caso de uso termina.



#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\- Não será um caso de uso muito frequente.

#### Questões em aberto

* O caso de uso deverá terminar, caso o prazo de candidaturas expire antes da atualização da candidatura?
* Qual a frequência de ocorrência deste caso de uso?


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC11_MD.svg](UC11_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O freelancer inicia a atualização de uma candidatura submetida.|	... interage com o utilizador? | AtualizarCandidaturaSubmetidaUI|  Pure Fabrication|
|  		 |... coordena o UC?|AtualizarCandidaturaSubmetidaController|Controller|
|      |...atualiza a instância candidatura?| Anuncio|Creator(Regra 1): no MD, o Anuncio recebe Candidaturas|
|      |...conhece o freelancer? | Plataforma | Contém todos os Freelancers.|
| | | RegistoFreelancer | Por aplicação de HC + LC|
| 2. O sistema apresenta ao freelancer a sua lista de candidaturas e solicita a seleção de uma.|... possui a lista de candidaturas do freelancer?|ListaCandidaturas|Pela aplicação do Creator (R1) as instâncias de Candidatura seriam criadas por Anuncio. Mas, por aplicação de HC+LC à classe Anuncio, esta delega essa responsabilidade na classe ListaCandidaturas.|
|      |...conhece Anuncio?|RegistoAnuncios|Pela aplicação do Creator (R1) as instâncias de Anuncio seriam criadas pela Plataforma. Mas, por aplicação de HC+LC à classe Plataforma, esta delega essa responsabilidade na classe RegistoAnuncios.|
| 3. O freelancer seleciona uma candidatura.||||
| 4. O sistema solicita os dados a alterar de modo a atualizar a candidatura(i.e. valor pretendido, numero dias, texto apresentacão (opcional), texto motivação (opcional)).||||  	
| 5. O freelancer escolhe os dados que pretende alterar e atualiza-os. | ...guarda os dados introduzidos? | Candidatura | IE: Candidatura conhece os seus próprios dados|		 					 
| 6. O sistema valida, apresenta os dados atualizados e pede a sua confirmação.|	...valida os dados da candidatura atualizada? (validação local)|ListaCandidaturas|IE: possui os seus próprios dados| 
||...valida os dados? (validação global) | Candidatura | IE: possui os seus próprios dados.| 
| 7. O freelancer atualiza os dados.||||
| 8. O sistema atualiza a candidatura e informa o freelancer do sucesso da operação. | ...guarda a candidatura atualizada? | ListaCandidaturas | IE: No MD oAnuncio recebe Candidaturas. Por HC+LC delega essa responsabilidade a ListaCandidaturas

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Anuncio
 * Candidatura
 


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * AtualizarCandidaturaSubmetidaUI  
 * AtualizarCandidaturaSubmetidaController
 * RegistoAnuncios
 * ListaCandidaturas


###	Diagrama de Sequência

![UC11_SD.svg](UC11_SD.svg)


###	Diagrama de Classes

![UC11_CD.svg](UC11_CD.svg)
