# UC 14 - Adjudicar Tarefa

## 1. Engenharia de Requisitos

### Formato Breve

O gestor de organização inicia uma nova adjudicação de tarefa. O sistema solicita dados (anúncio e através do tipo de regimento e tendo em consideração a seriação realizada, propõe ao gestor qual é o freelancer a que deve ser feita adjudicação). O gestor de organização seleciona o anúncio e o freelancer indicado pelo sistema. O sistema valida e apresenta os dados ao gestor de organização, pedindo que os confirme. O gestor de organização confirma. O sistema regista os dados e informa o gestor de organização do sucesso da operação.

### SSD
![UC14_SSD.svg](UC14_SSD.svg)


### Formato Completo

#### Ator principal

Gestor de Organização

#### Partes interessadas e seus interesses
* **Gestor de Organização:** pretende adjudicar tarefa para que possa posteriormente ser realizada tarefa.
* **T4J:** pretende que a plataforma permita adjudicar tarefas.


#### Pré-condições
n/a

#### Pós-condições
A informação da adjudicação do tarefa é registada no sistema.

### Cenário de sucesso principal (ou fluxo básico)

1. O gestor de organização inicia a adjudicação de uma tarefa .
2. O sistema mostra os anúncios publicadas em fase de atribuição não automática e que ainda não foram atribuidos e pede-lhe para escolher um.
3. O gestor de organização seleciona um anúncio.
4. O sistema através do tipo de regimento e tendo em consideração a seriação realizada, propõe ao gestor qual é o freelancer a que deve ser feita adjudicação.
5. O gestor da organização seleciona o freelancer indicado pelo sistema.
6. O sistema valida e apresenta os dados ao gestor de organização, pedindo que os confirme.
7. O gestor de organização confirma.
8. O sistema regista os dados e informa o gestor de organização do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O gestor de organização solicita o cancelamento da adjudicação do tarefa.

> O caso de uso termina.


6a. O sistema detecta que não foi selecionado nenhum freelancer.
>	1. O sistema informa que não foi selecionado nenhum freelancer.
>	2. O sistema permite a que seja selecionado o freelancer.
>
	>	2a. O gestor de organização não seleciona. O caso de uso termina.

6b. O sistema detecta que o freelancer introduzidos é inválido.
> 1. O sistema alerta o gestor de organização para o facto.
> 2. O sistema permite a sua alteração.
>
	> 2a. O gestor de organização não altera. O caso de uso termina.

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Existem outros dados que são necessários?
* Qual a frequência de ocorrência deste caso de uso?


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC14_MD.svg](UC14_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O gestor de organização inicia uma adjudicação de uma tarefa. |	... interage com o utilizador? | AdjudicarTarefaUI |  Pure Fabrication|
| |	... coordena o UC?	| AdjudicarTarefaController | Controller    |
| |...conhece o gestor a usar o sistema?|SessaoUtilizador|IE: cf. documentação do componente de gestão de utilizadores.|
| |...conhece o colaborador em questão?|ListaColaboradores|Pela aplicação do IE as instâncias de Colaborador seriam conhecidas pela Organizacao. Mas, por aplicação de HC+LC à Organizacao, esta delega essa responsabilidade à ListaColaboradores.|
| |...conhece a organização a que o colaborador pertence?|RegistoOrganizacoes|Pela aplicação do IE as instâncias da Organizacao seriam conhecidas pela Plataforma. Mas, por aplicação de HC+LC à Plataforma, esta delega essa responsabilidade à RegistoOrganizacoes.|
| |...conhece RegistoOrganizacoes?|Plataforma|IE: Plataforma possui RegistoOrganizacoes|
|  		 |	... cria instância de Adjudicaçao | Anuncio   | Creator (Regra1)no MD a Anuncio possui processo de atribuição  |
| 2. O sistema verifica quais são os anúncios cujo tipo de regimento segue critérios de seleção opcionais, que se encontram no período de atribuição e que não foram previamente atribuidos.| ...conhece os anuncios? | Plataforma | IE: no MD a Plataforma possui Anúncios.
|||RegistoAnuncios|IE: no MD a Plataforma possui Anuncios. Por aplicação de HC+LC delega a RegistoAnuncios.|
||...conhece os anúncios a serem atribuidos pelo sistema? | TipoRegimento| No MD, ProcessoAtribuicao ocorre em concordância com o TipoRegimento.
|||CaracterPA|No MD, o CaracterPA é referente a ProcessoAtribuicao.
|||Anuncio|No MD, Anuncio espoleta ProcessoAtribuicao.|
| 3. O sistema mostra os freelancers cujas candidaturas foram previamente seriadas e pede-lhe para escolher um (o sistema propõe o freelancer a escolher).|...conhece lista de candidaturas?| Anuncio | No MD, Anuncio recebe Candidaturas.|
| 4. O gestor de organização seleciona o freelancer (o freelancer proposto pelo sistema). |	... guarda os dados introduzidos?  |   Adjudicacao | Information Expert (IE) - instância criada no passo 1     |
| 5. O sistema regista os dados juntamente com a data/hora atual assim como um número único sequencial (por ano). O sistema obtém os dados necessários para a adjudicação da tarefa(org, freelancer, dsTarefa, dtInicioRealizacaoTarefa, nrDiasNecessarios, valorAceite, anuncio,numSeq, dataAdjudicacao). O sistema valida e apresenta os dados, pedindo ao gestor de organização para confirmar.
| 6. O gestor de organização confirma.   		 |							 |             |                              |
| 7. O sistema regista os dados e informa o gestor de organização do sucesso da operação.  		 |	... guarda a Adjudicacao criada? | Adjudicacao  | IE: No MD a Plataforma possui Adjudicacao |  


### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Anuncio
 * Candidatura
 * ProcessoSeriacao
 * ProcessoAtribuicao
 * TipoRegimento
 * Organizacao
 * Colaborador
 * Adjudicacao


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * AdjudicarTarefaUI
 * AdjudicarTarefaController
 * RegistoAnuncios
 * ListaCandidaturas
 * RegistoOrganizacoes
 * ListaColaboradores


###	Diagrama de Sequência

![UC14_SD.svg](UC14_SD.svg)


###	Diagrama de Classes

![UC14_CD.svg](UC14_CD.svg)
