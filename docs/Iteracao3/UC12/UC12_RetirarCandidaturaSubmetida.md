# UC12 - Retirar candidatura submetida

## 1. Engenharia de Requisitos

### Formato Breve

O freelancer inicia o retiramento de uma candidatura submetida. O sistema solicita os dados necessários (i.e. candidatura previamente submetida). O freelancer introduz os dados solicitados. O sistema apresenta os dados ao freelancer pedindo que o confirme. O freelancer confirma. O sistema efetua a operação de remoção e informa o administrativo do sucesso da operação.

### SSD
![UC12_SSD.svg](UC12_SSD.svg)


### Formato Completo

#### Ator principal

Freelancer

#### Partes interessadas e seus interesses
* **Freelancer** pretende que haja a possibilidade de retirar uma candidatura previamente submetida.
* **T4J:** pretende que os freelancer usufruam de uma maior flexibilidade, podendo retirar as candidaturas previamente submetidas.


#### Pré-condições
* O freelancer deve possuir candidaturas.


#### Pós-condições
* A candidatura escolhida pelo freelancer é removida do sistema.

### Cenário de sucesso principal (ou fluxo básico)

1. O freelancer inicia o retiramento de uma candidatura submetida.
2. O sistema apresenta ao freelancer a sua lista de candidaturas e solicita a seleção de uma.
3. O freelancer seleciona uma candidatura.
4. O sistema procede à remoção da candidatura e informa o freelancer do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O freelancer solicita o cancelamento do retiramento da candidatura submetida.

> O caso de uso termina.

4a. O sistema deteta que o anuncio para qual o freelancer se candidatou já não se encontra no período de candidaturas.
>	1. O sistema alerta o freelancer para o facto.

> 2. O caso de uso termina.



#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\- Não será um caso de uso muito frequente.

#### Questões em aberto

* O caso de uso deverá terminar, caso o prazo de candidaturas expire antes da remoção da candidatura?
* Deverá o freelancer continuar com a candidatura visível na aplicação, caso haja necessidade de fazer a mesma candidatura?
* Qual a frequência de ocorrência deste caso de uso?


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC12_MD.svg](UC12_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O inicia o retiramento de uma candidatura submetida.|	... interage com o utilizador? | RetirarCandidaturaSubmetidaUI|  Pure Fabrication|
|  		 |... coordena o UC?|RetirarCandidaturaSubmetidaController|Controller|
|      |...conhece o utilizador a usar o sistema?| SessaoUtilizador|IE: cf. documentação do componente de gestão de utilizadores.|
|     |...conhece o freelancer? RegistoFreelancers| Pela aplicação do IE as instâncias de Freelancer seriam conhecidas pela Plataforma. Mas, por aplicação de HC+LC à classe Plataforma, esta delega essa responsabilidade na classe RegistoFreelancers.
| 2. O sistema apresenta ao freelancer a sua lista de candidaturas e solicita a seleção de uma|... possui a lista de candidaturas do freelancer?|ListaCandidaturas|Pela aplicação do Creator (R1) as instâncias de Candidatura seriam criadas por Anuncio. Mas, por aplicação de HC+LC à classe Anuncio, esta delega essa responsabilidade na classe ListaCandidaturas.|
|      |...conhece Anuncio?|RegistoAnuncios|Pela aplicação do Creator (R1) as instâncias de Anuncio seriam criadas pela Plataforma. Mas, por aplicação de HC+LC à classe Plataforma, esta delega essa responsabilidade na classe RegistoAnuncios.|
| 3. O freelancer seleciona uma candidatura.||||
| 4. O sistema procede à remoção da candidatura e informa o freelancer do sucesso da operação.|	...retira a Candidatura do sistema?|ListaCandidaturas|Pela aplicação do IE a instância de Candidatura seria removida por Anuncio. Mas, por aplicação de HC+LC à classe Anuncio, esta delega essa responsabilidade na classe ListaCandidaturas.|  


### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Anuncio
 * Candidatura
 * Freelancer


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * DefinirAreaUI  
 * DefinirAreaController
 * RegistoAnuncios
 * ListaCandidaturas
 * RegistoFreelancers


###	Diagrama de Sequência

![UC2_SD.svg](UC2_SD.svg)


###	Diagrama de Classes

![UC2_CD.svg](UC2_CD.svg)
