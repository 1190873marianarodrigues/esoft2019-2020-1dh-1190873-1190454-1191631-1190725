# UC 13 - Seriar (Automaticamente) Candidaturas de Anúncio

## 1. Engenharia de Requisitos

### Formato Breve

O Timer espoleta o início do processo de seriação de anúncio para os anúncios cujo tipo de regimento segue critérios de seleção objetivos, que se encontrem no período de seriação, que não tenham sido previamente seriados e que possuam candidaturas efetuadas. O sistema classifica as candidaturas existentes a esse anúncio. O sistema valida e regista os dados juntamente com a data/hora atual. O sistema verifica a obrigatoriedade na realizacao do processo de atribuição. O sistema adjudica a tarefa. O sistema valida e regista os dados juntamente com a data/hora atual. O sistema repete o processo até que todos os anúncios seriaveis estejam seriados.

![UC13_SSD.svg](UC13_SSD.svg)

### Formato Completo

#### Ator principal

* Timer

#### Partes interessadas e seus interesses

* **Freelancer:** pretende conhecer a classificação das suas candidaturas à realização de determinados anúncios publicados na plataforma.
* **Organização:** pretende contratar pessoas externas (outsourcing) para a realização de determinadas tarefas e com competências técnicas apropriadas.
* **T4J:** pretende satisfazer as organizações e os freelancer facilitando a contratação de freelancers pelas organizações e vice-versa.

#### Pré-condições

* Existir pelo menos um anúncio de tarefa em condições de ser seriado automaticamente pelo sistema.

#### Pós-condições
* A informação do processo de seriação é registada no sistema.
* A informação do processo de atribuição, quando obrigatório, é registada no sistema.

#### Cenário de sucesso principal (ou fluxo básico)

1. O timer inicia o processo de seriação do anúncio.
2. O sistema verifica quais são os anúncios cujo tipo de regimento segue critérios de seleção objetivos, que se encontram no período de seriação, que não foram previamente seriados e que possue pelo menos uma candidatura efetuada. O sistema inicia o processo de seriação de um desses anúncios. O sistema ordena a lista de candidaturas desse anúncio. O sistema regista os dados, juntamente com a data/hora atual. O sistema verifica que esse anúncio tem atribuição obrigatória. O sistema inicia o processo de atribuição. O sistema obtém os dados necessários para a adjudicação da tarefa (i.e. organização, freelancer, descrição da tarefa adjudicada, data de início da atribuição do anúncio, data de fim da atribuição do anúncio, valor aceite por ambas as partes, id do anúncio, número sequencial, e data de adjudicação). O sistema regista os dados e informa o utilizador do sucesso da operação.

#### Extensões (ou fluxos alternativos)

2a. Não existem anúncios com as características descritas.
> O caso de uso termina.

2b. Não há candidaturas registadas no sistema para o anúncio em questão.
> 1. O sistema segue para o próximo anúncio.
> 1. O caso de uso prossegue.

2c. O sistema verifica que esse anúncio não tem atribuição obrigatória.
> 2. O caso de uso termina.


#### Requisitos especiais

\-

#### Lista de Variações de Tecnologias e Dados

\-

#### Frequência de Ocorrência

* Diariamente (02:15)

#### Questões em aberto

* O mesmo lugar/classificação pode ser atribuído a mais do que uma candidatura (e.g. em caso de empate)?
* O processo de seriação pode ser concluído havendo candidaturas por classificar?
* Há algum motivo que possa levar à desclassificação de uma candidatura?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC13_MD.scg](UC13_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
|1. O timer inicia o processo de seriação do anúncio. | ... cria o timer? | Plataforma | Creator (Regra 3): A plataforma possui os dados delay e intervalo usados para inicializar o Timer.|
|| ... cria a TimerTask? | Plataforma | Creator (Regra 3): A plataforma possui a informação necessária sobre a tarefa usada para inicializar a TimerTask. |
||...cria instância de ProcessoSeriacao?| Anuncio | Creator (Regra1): Anuncio espoleta ProcessoSeriacao.
|2. O sistema verifica quais são os anúncios cujo tipo de regimento segue critérios de seleção objetivos que se encontram no período de seriação, os que não foram previamente seriado, os que possuem candidaturas efetuadas e os que se encontrm no periodo de seriação. O sistema inicia o processo de seriação de um desses anúncios. O sistema ordena a lista de candidaturas desse anúncio. O sistema regista os dados, juntamente com a data/hora atual. O sistema verifica que esse anúncio tem atribuição obrigatória. O sistema inicia o processo de atribuição. O sistema obtém os dados necessários para a adjudicação da tarefa (i.e. organização, freelancer, descrição da tarefa adjudicada, data de início da atribuição do anúncio, data de fim da atribuição do anúncio, valor aceite por ambas as partes, id do anúncio, número sequencial, e data de adjudicação). O sistema regista os dados e informa o utilizador do sucesso da operação.| ...conhece os anuncios? | Plataforma | IE: no MD a Plataforma possui Anúncios.
|||RegistoAnuncios|IE: no MD a Plataforma possui Anuncios. Por aplicação de HC+LC delega a RegistoAnuncios.|
||...conhece o tipo de regimento? | Anuncio | No MD, Anúncio rege-se por TipoRegimento.
||...conhece o tipo de critérios pelos quais vai ser realizado o processo de seriação? | TipoRegimento| No MD, ProcessoSeriacao ocorre em concordância com o TipoRegimento.
|||Anuncio|No MD, Anuncio espoleta ProcessoSeriacao.|
||...conhece as candidaturas de cada anúncio?|Anuncio|No MD, Anuncio recebe Candidatura.
|||ListaCandidaturas|Por aplicação de HC+LC, Anuncio delega a responsabilidade a ListaCandidaturas.|
||...conhece o período de seriação?|Anuncio|No MD; o Anuncio tem dInicioSeriacao e dFimSeriacao|
||...cria a instância de ProcessoSeriacao?|Anuncio|No MD, Anuncio espoleta ProcessoSeriacao.
||...guarda os dados de Classificacao?|Classificacao|IE: possui os seus próprios dados.
||...guarda a instância de Classificacao criada |ProcessoSeriacao| No MD, ProcessoSeriacao resulta em Classificacao.
||...guarda o ProcessoSeriacao criado?|Anuncio|IE: no MD Anuncio espoleta ProcessoSeriacao.
||...verifica a obrigatoriedade da atribuição do anúncio? |TipoRegimento|No MD, o anúncio rege-se por TipoRegimento.
||...cria a instância de ProcessoAtribuicao?|Anuncio|No MD, Anuncio espoleta ProcessoAtribuicao.
||...conhece a primeira classificacao? |ProcessoSeriacao|No MD, ProcessoSeriacao resulta em Classificacao.
||...conhece a Candidatura?|Classificacao|No MD, Classificacao é referente a Candidatura.
||...conhece o Freelancer?|Candidatura|No MD, Candidatura é realizada por Freelancer.
||...conhece a tarefa? |Anuncio|No MD, Tarefa dá origem a Anuncio.
||...conhece a organização? |Plataforma|No MD, Plataforma tem registada Organizacao.
|||RegistoOrganizacoes|Por aplicação de HC+LC, plataforma delega a responsabilidade a RegistoOrganizacoes.|
||...cria a instância de Adjudicacao?|ProcessoAtribuicao|No MD, ProcessoAtribuicao resulta em Adjudicacao
||...guarda a Adjudicacao criada?|ProcessoAtribuicao|No MD, ProcessoAtribuicao resulta em Adjudicacao



### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Anuncio
 * Candidatura
 * ProcessoSeriacao
 * ProcessoAtribuicao
 * TipoRegimento
 * Classificacao
 * Tarefa
 * Adjudicacao
 * Organizacao

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * Timer
 * SeriarAnuncioCandidaturaTask
 * RegistoAnuncios
 * RegistoOrganizacoes
 * ListaCandidaturas

Outras classes:
  * Timer
  * TimerTask


###	Diagrama de Sequência
  * Este diagrama de sequência corresponde à criação de Timer e TimerTask.
![UC13_SD_CreateTimerTask.svg](UC13_SD_CreateTimerTask.svg)

  * Este diagrama de sequência corresponde ao detalhe do método seriarCandidaturasSubmetidas(), podendo ser considerado o diagrama principal.
![UC13_SD_SeriarCandidaturasSubmetidas.svg](UC13_SD_SeriarCandidaturasSubmetidas.svg)

  * Este diagrama de sequência corresponde a um dos possíveis algoritmos para busca dos anúncios seriáveis pelo sistema, tendo em conta quatro critérios.
![UC13_SD_GetAnunciosSeriaveisAutomaticos.svg](UC13_SD_GetAnunciosSeriaveisAutomaticos.svg)
  * Este diagrama de sequência corresponde à realização do processo de atribuição e consequente adjudicação de tarefa.
![UC13_SD_AtribuirTarefa.svg](UC13_SD_AtribuirTarefa.svg)


###	Diagrama de Classes

![UC13_CD.svg](UC13_CD.svg)
