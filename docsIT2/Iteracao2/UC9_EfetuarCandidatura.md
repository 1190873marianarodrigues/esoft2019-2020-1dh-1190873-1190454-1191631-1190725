# UC9 - Efetuar Candidatura

## 1. Engenharia de Requisitos

### Formato Breve

O freelancer inicia a efetuação de candidatura. O sistema solicita os dados (i.e. o anúncio desejado. o valor pretendido pela realização da tarefa, número de dias necessários para a realização da tarefa, texto de apresentação/motivação). O freelancer introduz os dados solicitados. O sistema apresenta os dados e solicita confirmação. O freelancer confirma os dados. O sistema **regista os dados** e informa o freelancer do sucesso da operação.

### SSD
![UC9_SSD.svg](UC9_SSD.svg)


### Formato Completo

#### Ator principal

* Freelancer

#### Partes interessadas e seus interesses
* **Organização:** pretende obter candidaturas para posteriorimente escolher o freelancer e assim ter a tarefa concluída.
* **T4J:** pretende que os freelancers se candidatem aos anúncios e que, assim, mantenham a plataforma funcional.
* **Freelancer:** permite poder efetuar candidatura aos anúncios para os quais é elegível.


#### Pré-condições
* O freelancer tem que estar registado na plataforma.
* O freelancer tem que possuir o grau mínimo exigido em todas as competências técnicas da tarefa que se propõe a realizar.

#### Pós-condições
* A candidatura do freelancer é registada no sistema.

### Cenário de sucesso principal (ou fluxo básico)

1. O freelancer inicia a efetuação da candidatura.
2. O sistema apresenta a lista de anúncios aos quais o freelancer se pode candidata e solicita a escolha do desejado.
3. O freelancer seleciona o anúncio desejado.
4. O sistema solicita os dados necessários (i.e. o valor pretendido pela realização da tarefa, o número de dias necessários) e o texto de apresentação/motivação.
5. O freelancer introduz os dados solicitados.
6. O sistema valida e apresenta os dados ao freelancer, pedindo que os confirme.
7. O freelancer confirma os dados.
8. O sistema regista os dados da candidatura e informa o freelancer do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O freelancer solicita o cancelamento da efetuação da candidatura.

> O caso de uso termina.

3a. O anúncio pretendido não está publicada no sistema.
> 1. O freelancer informa o sistema de tal facto. O caso de uso termina.

3b. Com os reconhecimentos que lhe foram atribuidos, o freelancer não tem opções para escolha de anúncios.
> 1. O sistema informa o freelancer de tal facto. O caso de uso termina

5a. O freelancer não introduz o texto de apresentação/motivação.
> 1. O sistema permite a continuação do caso de uso porque o texto de apresentação/motivação não é obrigatório.

6a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O freelancer não altera os dados. O caso de uso termina.

6b. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o freelancer para o facto.
> 2. O sistema permite a sua alteração (passo 3).
>
	> 2a. O freelancer não altera os dados. O caso de uso termina.

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Existem outros dados que são necessários?
* Aquando da criação de um anúncio, é gerado um id que permita a sua posterior identificação? Se sim, o sistema será simplificado.
* Como uma tarefa resulta num único anúncio e visto que não é referido no enunciado a existência de um identificador único de anúncio, poderá ser utilizada a referência da tarefa para a procura do anúncio em questão?
* Qual a frequência de ocorrência deste caso de uso?


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC9_MD.svg](UC9_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O freelancer inicia a efetuação da candidatura.   		 |	... interage com o utilizador? | EfetuarCandidaturaUI    |  Pure Fabrication: não se justifica atribuir esta responsabilidade a nenhuma classe existente no Modelo de Domínio. |
|  		 |	... coordena o UC?	| EfetuarCandidaturaController | Controller    |
||...conhece o utilizador a usar o sistema?| SessaoUtilizador|IE: cf. documentação do componente de gestão de utilizadores.|
||...conhece todos as instâncias de Freelancer? | RegistoFreelancers | IE: no MD, Plataforma tem/usa freelancers. No entanto, por aplicação de HC+LC à classe Plataforma, esta delega essa responsabilidade em RegistoFreelancers.
|  		 |	... cria instância de Candidatura?| ListaCandidaturas  | Pela aplicação do Creator (R1) as instâncias de Candidatura seriam criadas por Anuncio. Mas, por aplicação de HC+LC à classe Anuncio, esta delega essa responsabilidade na classe ListaCandidaturas.   |
| 2. O sistema apresenta a lista de anúncios aos quais o freelancer se pode candidatar. | ... conhece a lista de anúncios? | RegistoAnuncios | I.E. no MD a Plataforma possui todos os anúncios de todas as Organizações. Por aplicação de HC+LC, Plataforma delega essa responsabilidade no RegistoAnuncios.|
|3. O freelancer seleciona o anúncio desejado. |... guarda o anúncio selecionado? | Candidatura| IE: Anuncio possui a instância de Candidatura criada no passo 1 |
| 4. O  sistema solicita os dados necessários (i.e. o valor pretendido pela realização da tarefa, o número de dias necessários) e ainda o texto de apresentação/motivação.  		 |							 |             |                              |
| 5. O freelancer introduz os dados solicitados. 		 |	... guarda os dados introduzidos?  |   Candidatura | Information Expert (IE) - instância criada no passo 1: possui os seus próprios dados.     |
| 6. O sistema valida e apresenta os dados ao freelancer, pedindo que os confirme.   		 |	... valida os dados da Candidatura (validação local) | Candidatura |      IE: possui os seus próprios dados.|  	
|	 |	... valida os dados da Candidatura (validação global) | ListaCandidaturas  | IE: no MD, Anuncio possui Candidatura. No entanto, pela aplicação de HC+LC, a classe Anuncio delega essa responsabilidade em ListaCandidaturas |
| 7. O freelancer confirma os dados. |							 |             |                              |
| 8. O sistema regista os dados da candidatura e informa o freelancer do sucesso da operação. 		 |	... guarda a Candidatura criada? | ListaCandidaturas  | IE: No MD, Anuncio possui Candidatura. No entanto, pela aplicação de HC+LC, Anuncio delega a resposabilidade de guardar a Candidatura criada em ListaCandidaturas.|  



### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * RegistoAnuncios
 * RegistoFreelancers
 * RegistoOrganizacoes
 * Organizacao
 * ListaTarefas
 * Freelancer
 * Anuncio
 * ListaCandidaturas
 * Candidatura

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * EfetuarCandidaturaUI
 * EfetuarCandidaturaController

Outras classes de sistemas/componentes externos:

 * SessaoUtilizador


###	Diagrama de Sequência

![UC9_SD.svg](UC9_SD.svg)

![UC9_GetFreelancer_SD.svg](UC9_GetFreelancer_SD.svg)

![UC9_GetTarefaByRefTarefa_SD.svg](UC9_GetTarefaByRefTarefa_SD.svg)


###	Diagrama de Classes
![UC9_CD.svg](UC9_CD.svg)
