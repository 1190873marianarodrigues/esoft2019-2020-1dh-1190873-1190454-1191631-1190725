# UC7 - Registar Freelancer

## 1. Engenharia de Requisitos

### Formato Breve

O administrativo inicia o registo de um freelancer. O sistema solicita os dados necessários sobre o freelancer (i.e. nome do freelancer, NIF, endereço postal, contacto telefónico e email) bem como as suas habilitações académicas (grau, designação do curso, instituição que concedeu o grau e média do curso) e os reconhecimentos de competências técnicas. O administrativo introduz os dados solicitados. O sistema valida e apresenta os dados, pedindo que os confirme. O administrativo confirma. O sistema **regista os dados do freelancer, tornando este último um utilizador registado** e informa o administrativo do sucesso da operação.

### SSD
![UC7_SSD.svg](UC7_SSD.svg)


### Formato Completo

#### Ator principal

Administrativo

#### Partes interessadas e seus interesses
* **Administrativo:** pretende registar o freelancer para que possa usufruir das funcionalidade disponibilizadas pela plataforma.
* **T4J:** pretende que a organização em causa se registe de modo usar a plataforma.


#### Pré-condições
n/a

#### Pós-condições
A informação do registo é guardada no sistema.

#### Cenário de sucesso principal (ou fluxo básico)

1. O administrativo inicia o registo de um freelancer. 
2. O sistema solicita os dados básicos necessários sobre o freelancer (i.e. nome do freelancer, NIF, endereço postal, contacto telefónico e email)
3. O administrativo introduz os dados solicitados. 
4. O sistema solicita as habilitações académicas(grau, designação do curso, instituição.
5. O administrativo insere os dados das habilitações académicas. 
6. Os pontos 4 e 5 repetem-se até não haver mais habilitações.
7. O sistema apresenta a lista de competências técnicas para ser feito o seu reconhecimento.
8. O sistema solicita os dados para o reconhecimento(data, competencias tecnicas e grau de proficiencia).
9. O administrativo insere os dados para o reconhecimento. 
10. Os pontos 8 e 9 repetm-se até não haver mais reconhecimentos.
11. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.
12. O administrativo confirma.
13. O sistema regista o freelancer e informa o administrativo do sucesso da operação.

#### Extensões (ou fluxos alternativos)

*a. O administrativo solicita o cancelamento do registo do Freelancer.

> O caso de uso termina.

4a. Dados em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3, 5, 9)
>
	>	2a. O administrativo não altera os dados. O caso de uso termina.
	
4b. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3, 5, 9)
>
	>	2a. O administrativo não altera os dados. O caso de uso termina.

4c. O sistema deteta que existem dados introduzidos inválidos.
>	1. O sistema alerta o administrativo para o facto.
>	2. O sistema permite a sua alteração (passos 3, 5, 9)
>
	>	2a. O administrativo não altera os dados. O caso de uso termina.


#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Existem outros dados obrigatórios para além dos já conhecidos?
* É necessário existir algum mecanismo de segurança adicional para confirmar que a organização existe e é representada pela pessoa que a registou?
* Quais são as regras de segurança aplicaveis à palavra-passe?
* Qual a frequência de ocorrência deste caso de uso?


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC7_MD.svg](UC7_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
|1. O administrativo inicia o registo de um freelancer. |... interage com o utilizador?| RegistarFreelancerUI |Pure Fabrication|
| |... coordena o UC?| RegistarFreelancerController |Controller|
| |... cria instâncias de Freelancer?|RegistoFreelancer|Creator(regra1)|
|2. O sistema solicita os dados básicos necessários sobre o freelancer (i.e. nome do freelancer, NIF, endereço postal, contacto telefónico e email).||||
|3. O administrativo introduz os dados solicitados.  |... guarda os dados introduzidos?|Freelancer|IE: instância criada no passo 1|
|4. O sistema solicita as habilitações académicas(grau, designação do curso, instituição. |
|5.  O administrativo insere os dados das habilitações académicas.  | ...guarda os dados inseridos? | RegistoFreelancer | IE:instância criada no passo 1. |
|6. Os pontos 4 e 5 repetem-se até não haver mais habilitações.||||
|7. O sistema apresenta a lista de competências técnicas para ser feito o seu reconhecimento. |...conhece as comptências técnicas? | Plataforma | IE:A plataforma contem as competencias tecnicas|
|8. O sistema solicita os dados para o reconhecimento(data, competencias tecnicas e grau de proficiencia). ||||
|9. O administrativo insere os dados para o reconhecimento. |...guarda os dados inseridos? | RegistoFreelancer | IE:possui os seus proprios dados|
|10. Os pontos 8 e 9 repetm-se até não haver mais reconhecimentos.
|11. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. |...valida os dados do freelancer?(validação local) | RegistoFreelancer | IE: possui os seus proprios dados| 
| |...valida os dados do freelancer?(validaçao global) | Plataforma | IE: A plataforma possui freelancer.|
|12. O administrativo confirma.||||
|13. O sistema regista o freelancer e informa o administrativo do sucesso da operação. |...guarda o freelancer? | Plataforma | IE. No MD, a plataforma tem registados freelancers|

             

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Freelancer
 * HabilitacoesAcademicas
 * ReconhecimentoCompetencias 


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * RegistarFreelancerUI  
 * RegistarFreelancerController




###	Diagrama de Sequência

####    Diagrama principal

![UC7_SD.svg](UC7_SD.svg)

####    UC7_SD_RegistaFreelancerComoUtilizador

![UC7_SD_RegistaFreelancerComoUtilizador.svg](UC7_SD_RegistaFreelancerComoUtilizador.svg)



###	Diagrama de Classes

![UC7_CD.svg](UC7_CD.svg)

