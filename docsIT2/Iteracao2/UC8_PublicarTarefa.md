# UC8 - Publicar Tarefa

## 1. Engenharia de Requisitos

### Formato Breve

O colaborador da organização criou a tarefa inicia a publicação da mesma tarefa . O sistema solicita ao colaborador da organização os dados necessários (i.e. tarefa, período de	 publicitação	 da	 tarefa	 na	 plataforma, o	 período	 da apresentação	 de	 candidaturas	 pelos	 freelancers, o	 período	 de	 seriação e	decisão	de
	atribuição	da	tarefa	a	um	freelancer	pela	organização e tipo de	regimento aplicável). O colaborador da organização introduz os dados solicitados.
 O sistema apresenta os dados ao colaborador de Organização, pedindo que os confirme. O colaborador da organização confirma. O sistema regista os dados e informa
  o colaborador da organização do sucesso da operação.

### SSD
![UC8_SSD.svg](UC8_SSD.svg)


### Formato Completo

#### Ator principal

Colaborador da Organização

#### Partes interessadas e seus interesses
* **Colaborador da Organização:** pretende publicar a tarefa.
* **T4J:** pretende que colaborador consiga publicar tarefas.


#### Pré-condições
A tarefa já precisa de ter sido criada previamente a sua publicação.

#### Pós-condições
A informação da publicação da tarefa é registada no sistema.

### Cenário de sucesso principal (ou fluxo básico)

1. O colaborador de organização inicia a publicação da tarefa.
2. O sistema apresenta a lista de tarefas e solicita a escolha de uma por	si anteriormente criada.
3. O colaborador de organização seleciona uma tarefa.
4. O sistema solicita inico do período publicação, fim do período publicação, inico do período apr. candidatura ,fim do período apr. candidatura, inico do período seriação, fim do período seriação, tipo regimento.
5. O colaborador de organização insere os dados solicitados.
6. O sistema valida e apresenta os dados ao colaborador de organização, pedindo que os confirme.
7. O colaborador de organização confirma os dados.
8. O sistema regista os dados e informa o colaborador de organização do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O colaborador da organização solicita o cancelamento da definição da  publicação da tarefa.

> O caso de uso termina.

7a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O colaborador da organização não altera os dados. O caso de uso termina.

7b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o colaborador da organização para o facto.
>	2. O sistema permite a sua alteração (passo 3)
>
	>	2a. O colaborador da organização não altera os dados. O caso de uso termina.

7c. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o colaborador da organização para o facto.
> 2. O sistema permite a sua alteração (passo 3).
>
	> 2a. O colaborador da organização não altera os dados. O caso de uso termina.

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Existem outros dados que são necessários?
* Todos os dados são obrigatórios?
* Qual a frequência de ocorrência deste caso de uso?


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC8_MD.svg](UC8_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O colaborador de organização inicia a publicação da tarefa.   		 |	... interage com o utilizador? | PublicarTarefaUI    |  Pure Fabrication |
||... coordena o UC?	| PublicarTarefaController | Controller |
||...conhece o utilizador?/gestor a usar o sistema?|SessaoUtilizador|IE: cf. documentação do componente de gestão de utilizadores.|
||...conhece a organização a que o colaborador pertence?|RegistoOrganizacoes?| Tem registadas todas as organizações|
||...conhece RegistoOrganizacoes|Plataforma|Plataforma possui RegistoOrganizacoes|
||... cria instância de Anuncio?| Colaborador   | Creator (Regra1) O colaborador publica a tarefa |
| 2. O sistema apresenta a lista de tarefas do colaborador e solicita a escolha de uma.|...conhece o colaborador?| Organizacao | Organizacao tem colaboradores|
||...conhece a lista de tarefas do colaborador?|ListaTarefa| IE: no MD ListaTarefas tem registadas todas as instâncias de tarefa|
||...conhece ListaTarefas|Oraganização|IE: no MD Organizacao possui a instância de ListaTarefas|
|3. O colaborador de organização seleciona uma tarefa.
|4. O sistema solicita os dados necessários (i.e. inico do período publicação, fim do período publicação, inico do período apr. candidatura ,fim do período apr. candidatura, inico do período seriação, fim do período seriação, tipo regimento).
|5. O colaborador da organização introduz os dados solicitados.  		 |	... guarda os dados introduzidos?  | Anuncio |Information Expert (IE)-instância criada no passo 1: possui os seus próprios dados.     |
|6. O sistema valida e apresenta os dados ao colaborador de organização, pedindo que os confirme.|... valida os dados introduzidos (validação global)? |IE: a Plataforma contém/agrega Anúncio.  ||
|7. O colaborador da organização confirma. ||||
|8. O sistema regista os dados e informa o colaborador de organização do sucesso da operação.|... guarda publicação da tarefa?| Plataforma |IE: a Plataforma contém/agrega Anuncio. |

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Organizacao
 * Colaborador
 * RegistoOrganizacoes
 * Tarefa
 * Anuncio
 * SessaoUtilizador

Outras classes de software (i.e. Pure Fabrication) identificadas:

 * PublicarTarefaUI
 * PublicarTarefaController

 ###	Diagrama de Sequência

 #### Diagrama Principal

 ![UC8_SD.svg](UC8_SD.svg)

 #### UC8_GetListaTarefas_SD

 ![UC8_GetListaTarefas_SD.svg](UC8_GetListaTarefas_SD.svg)

 #### UC8_GetTarefa_SD

 ![UC8_GetTarefa_SD.svg](UC8_GetTarefa_SD.svg)





 ###	Diagrama de Classes

 ![UC8_CD.svg](UC8_CD.svg)
