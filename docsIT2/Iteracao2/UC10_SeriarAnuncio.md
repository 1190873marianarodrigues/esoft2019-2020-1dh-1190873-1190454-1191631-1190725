# UC10 - Seriar Anúncio

## 1. Engenharia de Requisitos

### Formato Breve

O colaborador de organização responsável inicia a seriação de um anuncio. O sistema solicita ao colaborador de organização os dados (i.e. anuncio, data/hora do processo de seriação, participantes no processo de seriação, candidaturas, lugar das candidaturas). O colaborador de organização introduz os dados solicitados. O sistema apresenta os dados ao colaborador de organização e solicita confirmação. O colaborador de organização confirma os dados. O sistema informa ao colaborador de organização do sucesso da operação.

### SSD
![UC10_SSD.svg](UC10_SSD.svg)


### Formato Completo

#### Ator principal

Colaborador de Organização

#### Partes interessadas e seus interesses
* **Colaborador de Organização:** pretende seriar um anúncio anteriormente publicado pelo mesmo.
* **T4J:** pretende que o colaborador em causa em causa faça a seriação do anúncio.
* **Freelancer:** pretende que a classificação das candidaturas saia, para saber se lhe foi atribuída a tarefa ou não.
* **Organização:** pretende que o freelancer a quem foi atribuída a tarefa inicie a execução da mesma.


#### Pré-condições
* O anúncio em causa tem que estar registado.

#### Pós-condições
* As candidaturas são classificadas.
* O processo de seriação é registado no sistema.


#### Cenário de sucesso principal (ou fluxo básico)

1. O colaborador de organização inicia a seriação de um anúncio.
2. O sistema apresenta a lista de anúncios do colaborador em questão e solicita a escolha de um.
3. O colaborador de organização seleciona um anúncio.
4. O sistema solicita a introdução de um dado(dataHora).
5. O colaborador de organização introduz o dado solicitado.
6. O sistema mostra a lista de colaboradores da organização do ator.
7. O colaborador de organização seleciona um colaborador.
8. O passo 7 repete-se enquanto não forem selecionados todos os colaboradores participantes.
9. O sistema mostra a lista de candidaturas.
10. O colaborador de organização escolhe uma candidatura.
11. O sistema solicita a introdução de um dado(lugar).
12. O colaborador de organização introduz o dado solicitado.
13. Os passos 10 a 12 repetem-se até à última candidatura.
14. O sistema valida e apresenta os dados ao colaborador de organização, pedindo que os confirme.
15. O colaborador de organização confirma os dados.
16. O sistema regista os dados e informa ao colaborador de organização do sucesso da operação.

#### Extensões (ou fluxos alternativos)

*a. O colaborador de organização solicita o cancelamento da seriação.

> O caso de uso termina.

14a. O sistema deteta que alguns dados devem ser únicos no sistema.
> 1. O sistema alerta o colaborador de organização para o facto.
> 2. O sistema permite a sua alteração (passo 3).
>
	> 2a. O colaborador de organização não altera os dados. O caso de uso termina.

14b. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O utilizador não registado não altera os dados. O caso de uso termina.

14c. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o colaborador de organização para o facto.
> 2. O sistema permite a sua alteração (passo 3).
>
	> 2a. O colaborador de organização não altera os dados. O caso de uso termina.





#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Existem outros dados obrigatórios para além dos já conhecidos?
* É necessário existir algum mecanismo de segurança adicional para confirmar que o colaborador que vai seriar o anúncio é o mesmo que o publicou?
* Qual a frequência de ocorrência deste caso de uso?
* Caso a seriação seja automática, o processo pode decorrer sem a intervenção do colaborador de Organização?


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC10_MD.svg](UC10_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
|1. O colaborador de organização inicia a seriação de um anúncio|... interage com o colaborador de organização?| SeriarAnuncioUI |Pure Fabrication|
| |... coordena o UC?| SeriarAnuncioController |Controller|
| |...conhece o utilizador/gestor a usar o sistema?|SessaoUtilizador|IE: cf. documentação do componente de gestão de utilizadores.|
| |...conhece a organização a que o colaborador pertence?|RegistoOrganizacoes|Pela aplicação do IE as instâncias da Organizacao seriam conhecidas pela Plataforma. Mas, por aplicação de HC+LC à Plataforma, esta delega essa responsabilidade à RegistoOrganizacoes.|
| |...conhece RegistoOrganizacoes?|Plataforma|IE: Plataforma possui RegistoOrganizacoes|
| |...cria instância de ProcessoSeriacao?|Anuncio|Creator (Regra1): no MD a Anuncio possui ProcessoSeriacao|
|2. O sistema apresenta a lista de anúncios do colaborador em questão e solicita a escolha de um.|...conhece a lista de anúncios?|RegistoAnuncios|Pela aplicação do IE as instâncias da Anuncio seriam conhecidas pela Plataforma. Mas, por aplicação de HC+LC à Plataforma, esta delega essa responsabilidade à RegistoAnuncios.|
| |...conhece RegistoAnuncios|Plataforma|IE: Plataforma possui a instância de RegistoAnuncios|
|3. O colaborador de organização seleciona um anúncio.||||
|4. O sistema solicita a introdução de um dado(dataHora).||||
|5. O colaborador de organização introduz o dado solicitado.|...guarda o dado introduzido?|ProcessoSeriacao|IE: ProcessoSeriacao possui os seus proprios dados.|
|6. O sistema mostra a lista de colaboradores da organização do ator.|...conhece a lista de colaboradores?|Organizacao|IE: no MD uma Organizacao possui todos as instâncias Colaborador que pertencem a essa Organizacao.|
|7. O colaborador de organização seleciona um colaborador.|guarda o colaborador selecionado?|ProcessoSeriacao|IE: no MD Colaborador participa em ProcessoSeriacao|
|8. O passo 7 repete-se enquanto não forem selecionados todos os colaboradores participantes.||||
|9. O sistema mostra a lista de candidaturas.|...conhece a lista de candidaturas?|ListaCandidaturas|IE: no MD ListaCandidaturas tem listadas todas as instâncias Candidatura referente a um Anuncio|
| |...conhece ListaCandidaturas|Anuncio|IE: no MD Anuncio possui a instância ListaCandidaturas|
|10. O colaborador de organização escolhe uma candidatura.|...cria instância de Classificacao?|ProcessoSeriacao|Creator (Regra1): no MD ProcessoSeriacao tem Classificacao|
|11. O sistema solicita a introdução de um dado(lugar).||||
|12. O colaborador de organização introduz o dado solicitado.|...guarda os dados solicitados?|Classificacao|IE: possui os seus próprios dados.|
| |...valida a Classificacao?(validação local)|Classificacao|IE: possui os seus próprios dados|
| |...guarda a Classificacao criada?|ProcessoSeriacao|IE: no MD ProcessoSeriacao tem Classificacao|
|13. Os passos 10 a 12 repetem-se até à última candidatura.||||
|14. O sistema valida e apresenta os dados ao colaborador de organização, pedindo que os confirme.|...valida os dados de ProcessoSeriacao?(validação local)|ProcessoSeriacao|IE: possui os seus próprios dados|
|15. O colaborador de organização confirma os dados.||||
|16. O sistema regista os dados e informa ao colaborador de organização do sucesso da operação.|...guarda o ProcessoSeriacao criado?|Anuncio|IE: no MD Anuncio tem ProcessoSeriacao|


### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Organizacao
 * Colaborador
 * RegistoOrganizacoes
 * RegistoAnuncios
 * Anuncio
 * ListaCandidaturas
 * Candidatura
 * Classificacao

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * SeriarAnuncioUI  
 * SeriarAnuncioController

Outras classes de sistemas/componentes externos:

 * SessaoUtilizador


###	Diagrama de Sequência

#### Diagrama Principal

![UC10_SD.svg](UC10_SD.svg)

#### UC10_GetListaAnuncios_SD

![UC10_GetListaAnuncios_SD.svg](UC10_GetListaAnuncios_SD.svg)

#### UC10_GetAnuncio_SD

![UC10_GetAnuncio_SD.svg](UC10_GetAnuncio_SD.svg)

#### UC10_GetListaColaboradores_SD

![UC10_GetListaColaboradores_SD.svg](UC10_GetListaColaboradores_SD.svg)

#### UC10_GetListaCandidaturas_SD

![UC10_GetListaCandidaturas_SD.svg](UC10_GetListaCandidaturas_SD.svg)




###	Diagrama de Classes

![UC10_CD.svg](UC10_CD.svg)
