# Projeto de ESOFT 2019-2020


# 1. Constituição do Grupo de Trabalho ###

O grupo de trabalho é constituído pelo estudantes identificados na tabela seguinte.

| Aluno Nr.	   | Nome do Aluno			    |
|--------------|------------------------------|
| **1190873**  | Mariana Rodrigues             |
| **1190454**  | Bruno Gonçalves              |
| **1191631**  | Ricardo Moreira             |
| **1190725**  | João Miranda             |



# 2. Distribuição de Tarefas ###

A distribuição de tarefas ao longo do período de desenvolvimento do projeto pelos elementos do grupo de trabalho realizou-se conforme descrito na tabela seguinte.

**Esta tabela deve estar sempre atualizada.**

| Tarefa                      | [Iteração 1](Iteracao1/README.md) | [Iteração 2](Iteracao2/README.md) | [Iteração 3](Iteracao1/README.md) |
|-----------------------------|------------|------------|------------|
| Diagrama Casos de Uso (DUC) |  [fornecido](Iteracao1/DUC.md)   |   [todos](Iteracao2/DUC.md)  |   [todos](Iteracao3/DUC.md)  |
| Glossário  |  [todos](Iteracao1/Glossario.md)   |   [todos](Iteracao2/Glossario.md)  |   [todos](Iteracao3/Glossario.md)  |
| Especificação Suplementar   |   (n/a)    |   [todos](Iteracao2/FURPS.md)  |   [todos](Iteracao3/FURPS.md)  |
| Modelo de Domínio           |  [todos](Iteracao1/MD.md)   |   [todos](Iteracao2/MD.md)  |   [todos](Iteracao3/MD.md)  |
| UC 1 (Requisitos + Design)  |  [fornecido](Iteracao1/UC1_RegistarOrganizacao.md)   |  [1190454](Iteracao2/UC1_RegistarOrganizacao.md)          |            |
| UC 2 (Requisitos + Design)  |  [fornecido](Iteracao1/UC2_DefinirArea.md)   |  [INALTERADO](Iteracao2/UC2_DefinirArea.md)          |            |
| UC 3 (Requisitos + Design)  |  [fornecido](Iteracao1/UC3_DefinirCategoria.md)   |  [1191631](Iteracao2/UC3_DefinirCategoria.md)          |            |
| UC 4 (Requisitos + Design)  |  [fornecido](Iteracao1/UC4_EspecificarCT.md)   |  [1190873](Iteracao2/UC4_EspecificarCT.md)          |            |
| UC 5 (Requisitos + Design)  |  [fornecido](Iteracao1/UC5_EspecificarColaborador.md)   | [1190725](Iteracao2/UC5_EspecificarColaborador.md)           |            |
| UC 6 (Requisitos + Design)  |  [fornecido](Iteracao1/UC6_EspecificarTarefa.md)   |  [1190725](Iteracao2/UC6_EspecificarTarefa.md)          |            |
| UC 7 (Requisitos + Design)  |     |  [1190725](Iteracao2/UC7_RegistarFreelancer.md)          |            |
| UC 8 (Requisitos + Design)  |     |  [1191631](Iteracao2/UC8_PublicarTarefa.md)          |            |
| UC 9 (Requisitos + Design)  |     |  [1190873](Iteracao2/UC9_EfetuarCandidatura.md)          |            |
| UC 10 (Requisitos + Design)  |     | [1190454](Iteracao2/UC10_SeriarAnuncio.md)           |            |
| ...                         |    ...     |            |            |


