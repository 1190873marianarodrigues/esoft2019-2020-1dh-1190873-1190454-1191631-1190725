/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Plataforma {

    private String m_strDesignacao;
    private final AutorizacaoFacade m_oAutorizacao;
    private final Set<CategoriaTarefa> m_lstCategoriasTarefa;
    private RegistoOrganizacoes ro;
    private RegistoAnuncios ra;
    private RegistoFreelancers rf;
    private RegistoCompetenciasTecnicas rct;
    private RegistoAreasAtividade rat;
    private Set<Freelancer> m_lstFreelancers;
    private Set<CompetenciaTecnica> lstComp;

    public Plataforma(String strDesignacao) {
        if ((strDesignacao == null)
                || (strDesignacao.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.m_strDesignacao = strDesignacao;
        this.m_oAutorizacao = new AutorizacaoFacade();
        this.m_lstCategoriasTarefa = new HashSet<>();
        this.ro = new RegistoOrganizacoes(this.m_oAutorizacao);
        this.ra = new RegistoAnuncios();
        this.rf = new RegistoFreelancers();
        this.rat = new RegistoAreasAtividade();
        this.rct = new RegistoCompetenciasTecnicas();
    }

    public AutorizacaoFacade getAutorizacaoFacade() {
        return this.m_oAutorizacao;
    }

    // </editor-fold>
    // Areas de Atividade 
    // <editor-fold defaultstate="collapsed">
    // </editor-fold>
    public Set<CategoriaTarefa> getListaCategoriasTarefa() {
        return m_lstCategoriasTarefa;
    }

    public CategoriaTarefa getCategoriaById(String catId) {
        // Escrever codigo para encontrar a categoria através do id passado por parametro

        //Em baixo segue um exemplo de instancia de CategoriaTarefa somente para teste
        return new CategoriaTarefa();
    }

    public AreaAtividade getAreaAtividadeByID(String areaAtividadeId) {
        // Escrever codigo para encontrar a área de atividade através do id passado por parametro

        //Em baixo segue um exemplo de instancia de AreaAtividade somente para teste
        return new AreaAtividade("código", "descrição breve", "descrição detalhada");
    }

    public CompetenciaTecnica getCompetenciaTecnicaById2(String compTecnicaId) {
        // Escrever codigo para encontrar a competência técnica através do id passado por parametro

        //Em baixo segue um exemplo de instancia de CompetenciaTecnica somente para teste
        return new CompetenciaTecnica("ID", "descrição breve", "descriçao detalhada", new AreaAtividade("código", "descrição breve", "descrição detalhada"), "obrigatório");
    }

    public RegistoOrganizacoes getRegistoOrganizacoes() {
        return ro;
    }

    public RegistoAnuncios getRegistoAnuncios() {
        return ra;
    }

    public RegistoFreelancers getRegistoFreelancers() {
        return rf;
    }

    public RegistoCompetenciasTecnicas getRegistoCompetenciasTecnicas() {
        return rct;
    }

    public RegistoAreasAtividade getRegistoAreaAtividade() {
        return rat;
    }

    public boolean addFreelancer(Freelancer freelancer) {
        return m_lstFreelancers.add(freelancer);
    }

    public boolean validaFreelancer(Freelancer freelancer) {
        //escrever codigo validaçao
        return true;
    }

    public RegistoFreelancers getRegistoFreelancer() {
        return rf;
    }
    
    private Set <CompetenciaTecnica> getListaCompTecnicas(){
        return lstComp;
    }

    public int getNumeroSequencial() {
        //escrever código de geração de um número sequencial único e devolvê-lo
        
        return 1;
    }
    
    

}
