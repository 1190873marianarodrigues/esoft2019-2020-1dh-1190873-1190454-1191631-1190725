/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author marianarodrigues163
 */
public class RegistoAreasAtividade {
    
    private final Set<AreaAtividade> m_lstAreasAtividade;

    public RegistoAreasAtividade() {
       this.m_lstAreasAtividade = new HashSet<>();
    }
    
    public AreaAtividade getAreaAtividadeById(String strId)
    {
        for(AreaAtividade area : this.m_lstAreasAtividade)
        {
            if (area.hasId(strId))
            {
                return area;
            }
        }
        
        return null;
    }

    public AreaAtividade novaAreaAtividade(String strCodigo, String strDescricaoBreve, String strDescricaoDetalhada)
    {
        return new AreaAtividade(strCodigo, strDescricaoBreve,strDescricaoDetalhada);
    }

    public boolean registaAreaAtividade(AreaAtividade oArea)
    {
        if (this.validaAreaAtividade(oArea))
        {
            return addAreaAtividade(oArea);
        }
        return false;
    }

    private boolean addAreaAtividade(AreaAtividade oArea)
    {
        return m_lstAreasAtividade.add(oArea);
    }
    
    public boolean validaAreaAtividade(AreaAtividade oArea)
    {
        boolean bRet = true;
        
        // Escrever aqui o código de validação
        
        //
      
        return bRet;
    }
    
    public List<AreaAtividade> getAreasAtividade()
    {
        List<AreaAtividade> lc = new ArrayList<>();
        lc.addAll(this.m_lstAreasAtividade);
        return lc;
    }
    
}
