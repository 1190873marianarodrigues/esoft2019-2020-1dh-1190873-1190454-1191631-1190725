/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.ui.console;

import java.util.ArrayList;
import java.util.List;
import pt.ipp.isep.dei.esoft.pot.controller.DefinirCategoriaController;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

/**
 *
 * @author joaom
 */
public class DefinirCategoriaUI
{
    private DefinirCategoriaController m_controller;
    public DefinirCategoriaUI()
    {
        m_controller = new DefinirCategoriaController();
    }

    public void run()
    {
        System.out.println("\nDefinir Categoria de Tarefa:");

        if(introduzDados())
        {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.registaCategoriaTarefa()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir o registo com sucesso.");
                }
            }
        }
        else
        {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }
    
    private boolean introduzDados() {
        String identInterno = Utils.readLineFromConsole("Identificador Interno: ");
        String descriçao = Utils.readLineFromConsole("Descrição: ");
        String areaAtividade = Utils.readLineFromConsole("Área de atividade: ");
        ArrayList<String> compTecnicas = new ArrayList<String>();
        String op;
        do{
            compTecnicas.add(Utils.readLineFromConsole("Escolha uma competência técnica: "));
            op = Utils.readLineFromConsole("Deseja adicionar mais alguma competência? (S/N)");
        }while(op.equalsIgnoreCase("S"));
        
        
        return m_controller.novaCategoriaTarefa(identInterno, descriçao, areaAtividade, compTecnicas);
    }
    
    private void apresentaDados() 
    {
        System.out.println("\nInformação a registar:\n" + m_controller.getCategoriaTarefaString());
    }
      
}