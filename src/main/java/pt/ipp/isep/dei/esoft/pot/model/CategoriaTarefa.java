/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Set;

/**
 *
 * @author user
 */
public class CategoriaTarefa {

    public CategoriaTarefa() {
    }

    
    
    private String identInterno;
    private String descriçao;
    private AreaAtividade areaAtividade;
    private Set<CompetenciaTecnica> compTecnicas;

    public CategoriaTarefa(String identInterno, String descriçao, AreaAtividade areaAtividade, Set<CompetenciaTecnica> compTecnicas) {
        if ((identInterno == null) || (descriçao == null) || (areaAtividade == null) || (compTecnicas == null)
                || (identInterno.isEmpty()) || (descriçao.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.identInterno = identInterno;
        this.descriçao = descriçao;
        this.areaAtividade = areaAtividade;
        this.compTecnicas = compTecnicas;

    }

    @Override
    public String toString() {
        return "Categoria Tarefa{" + "identificador interno: " + identInterno + "descrição: " + descriçao + "Área de Atividade: " + areaAtividade + "Competências técnicas: " + compTecnicas;

    }

}
