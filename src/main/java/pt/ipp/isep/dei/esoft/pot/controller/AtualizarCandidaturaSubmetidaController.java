/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.ArrayList;
import java.util.List;
import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.model.Candidatura;
import pt.ipp.isep.dei.esoft.pot.model.Freelancer;
import pt.ipp.isep.dei.esoft.pot.model.ListaCandidaturas;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.RegistoAnuncios;
import pt.ipp.isep.dei.esoft.pot.model.RegistoFreelancers;

/**
 *
 * @author user
 */
public class AtualizarCandidaturaSubmetidaController {

    private final AplicacaoPOT app;
    private final Plataforma plataforma;
    private SessaoUtilizador sessao;
    private String email;

    public AtualizarCandidaturaSubmetidaController() {
        this.app = AplicacaoPOT.getInstance();
        this.plataforma = this.app.getPlataforma();
    }

    public List<Candidatura> getListaCandidaturasFreelancer() {
        List<Candidatura> lcf = new ArrayList<Candidatura>();

        RegistoFreelancers rFreel = this.plataforma.getRegistoFreelancer();
        RegistoAnuncios ra = this.plataforma.getRegistoAnuncios();

        this.sessao = this.app.getSessaoAtual();
        this.email = this.sessao.getEmailUtilizador();

        Freelancer freel = rFreel.getFreelancerByEmailUtlz(this.email);

        List<Anuncio> lstAnuncios = ra.getListaAnuncios();

        for (int i = 0; i < lstAnuncios.size(); i++) {
            Anuncio anuncio = lstAnuncios.get(i);
            boolean periodoCandidatura = false;
            periodoCandidatura = anuncio.isAnuncioPeriodoCandidaturas();

            if (periodoCandidatura) {
                ListaCandidaturas lc = anuncio.getListaCandidaturas();
                List<Candidatura> lstCandidaturas = lc.getLstCandidaturas();

                for (int j = 0; j < lstCandidaturas.size(); i++) {
                    boolean candidaturaFreelancer = false;
                    Candidatura candi = lstCandidaturas.get(j);
                    candidaturaFreelancer = candi.isCandidaturaFromFreelancer(freel);

                    if (candidaturaFreelancer) {
                        lcf.add(candi);
                    }
                }
            }
        }
        return lcf;
    }

    public boolean atualizaCandidatura() {
        return true;
    }
    
    public boolean setDados(valorPrt,nrDias,txtApres,txtMotiv){
        return true;
    }
}
