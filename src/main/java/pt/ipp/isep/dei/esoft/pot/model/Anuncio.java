/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Date;
import java.util.Set;

/**
 *
 * @author user
 */
public class Anuncio {

    private Date inicioPeriodoPublicitacao;
    private Date fimPeriodoPublicitacao;
    private Date inicioPeriodoApresentacaoCandidaturas;
    private Date fimPeriodoApresentacaoCandidaturas;
    private Date inicioPeriodoSeriacao;
    private Date fimPeriodoSeriacao;
    private String tipoRegimento;
    private ProcessoSeriacao processoS;
    private ProcessoAtribuicao processoA;
    private ListaCandidaturas lc;
    private TipoRegimento tipoReg;
    private Tarefa tarefa;

    public Anuncio(Date inicioPeriodoPublicitacao, Date fimPeriodoPublicitacao, Date inicioPeriodoApresentacaoCandidaturas, Date fimPeriodoApresentacaoCandidaturas, Date inicioPeriodoSeriacao, Date fimPeriodoSeriacao, String tipoRegimento) {
        this.inicioPeriodoPublicitacao = inicioPeriodoPublicitacao;
        this.fimPeriodoPublicitacao = fimPeriodoPublicitacao;
        this.inicioPeriodoApresentacaoCandidaturas = inicioPeriodoApresentacaoCandidaturas;
        this.fimPeriodoApresentacaoCandidaturas = fimPeriodoApresentacaoCandidaturas;
        this.inicioPeriodoSeriacao = inicioPeriodoSeriacao;
        this.fimPeriodoSeriacao = fimPeriodoSeriacao;
        this.tipoRegimento = tipoRegimento;
    }

    public Anuncio() {

    }

    public ProcessoSeriacao novoProcessoSeriacao(Colaborador colab) {
        try {
            return new ProcessoSeriacao(colab);
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean validaProcessoSeriacao(ProcessoSeriacao processoS) {
        //escrever código da validação do processo de seriação
        return true;
    }

    private boolean setProcessoSeriacao(ProcessoSeriacao processoS) {
        this.processoS = processoS;
        return true;
    }

    public boolean registarProcessoSeriacao(ProcessoSeriacao processoS) {
        if (validaProcessoSeriacao(processoS) && processoS.valida()) {
            return setProcessoSeriacao(processoS);
        } else {
            return false;
//            throw new Exception("Erro ao registar o processo de seriação");
        }
    }

    public ListaCandidaturas getListaCandidaturas() {
        return lc;
    }

    public ProcessoAtribuicao novoProcessoAtribuicao(Date dataHora) {
        try {
            return new ProcessoAtribuicao(dataHora);
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean validaProcessoAtribuicao(ProcessoAtribuicao processoA) {
        //escrever código da validação do processo de seriação
        return true;
    }

    private boolean addProcessoAtribuicao(ProcessoAtribuicao processoA) {
        this.processoA = processoA;
        return true;
    }

    public boolean registarProcessoAtribuicao(ProcessoAtribuicao processoA) {
        if (validaProcessoAtribuicao(processoA)) {
            return addProcessoAtribuicao(processoA);
        } else {
            return false;
//            throw new Exception("Erro ao registar o processo de seriação");
        }
    }

    public boolean isAnuncioPeriodoCandidaturas() {
        /**
         * Escrever código para se saber se o anúncio se encontra no período de
         * candidaturas. Return true se sim, return false se não.
         */

        return true;
    }

    public TipoRegimento getTipoReg() {
        return tipoReg;
    }

    public Tarefa getTarefa() {
        return tarefa;
    }

    public ProcessoSeriacao novoProcessoSeriacao() {
        try {
            return new ProcessoSeriacao();
        } catch (Exception e) {
            throw e;
        }
    }

    public ProcessoAtribuicao novoProcessoAtribuicao() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Candidatura AtualizaCandidatura() {

        return null;
    }
}
