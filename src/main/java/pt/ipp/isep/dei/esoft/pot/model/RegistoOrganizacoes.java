/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.HashSet;
import java.util.Set;
import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;

/**
 *
 * @author user
 */
public class RegistoOrganizacoes {
    
    private final Set<Organizacao> m_lstOrganizacoes;
    private final AutorizacaoFacade m_oAutorizacao;

    // Organizações
    
    // <editor-fold defaultstate="collapsed">
    
    public RegistoOrganizacoes(AutorizacaoFacade m_oAutorizacao) {
        this.m_lstOrganizacoes = new HashSet<Organizacao>();
        this.m_oAutorizacao = m_oAutorizacao;
    }
    
    
    public Organizacao novaOrganizacao(String strNome, String strNIF, String strWebsite,String strTelefone, String strEmail, EnderecoPostal oMorada, Colaborador oGestor)
    {
        return new Organizacao(strNome,strNIF, strWebsite, strTelefone, strEmail, oMorada, oGestor);
    }

    public boolean registaOrganizacao(Organizacao oOrganizacao)
    {
        if (this.validaOrganizacao(oOrganizacao))
        {
            Colaborador oGestor = oOrganizacao.getGestor();
            String strNomeGestor = oGestor.getNome();
            String strEmailGestor = oGestor.getEmail();
            /* pwd é criada usando um algoritmo externo que possua um adaptador que implemente AlgoritmoGeradorPasswords
               Como esses algoritmos são desconhecidos de momento, pwd será uma string "default".
            */
            String strPwd = "default";
            if (this.m_oAutorizacao.registaUtilizadorComPapeis(strNomeGestor,strEmailGestor, strPwd, 
                    new String[] {Constantes.PAPEL_GESTOR_ORGANIZACAO,Constantes.PAPEL_COLABORADOR_ORGANIZACAO}))
                return addOrganizacao(oOrganizacao);
        }
        return false;
    }

    private boolean addOrganizacao(Organizacao oOrganizacao)
    {
        return m_lstOrganizacoes.add(oOrganizacao);
    }
    
    public boolean validaOrganizacao(Organizacao oOrganizacao)
    {
        boolean bRet = true;
        
        // Escrever aqui o código de validação
        if (this.m_oAutorizacao.existeUtilizador(oOrganizacao.getGestor().getEmail()))
            bRet = false;
      
      
        return bRet;
    }
    
    public Organizacao getOrganizacaoByIdUtilizador(String idUtilizador) {

        // Escrever código para procurar e devolver a organização a que pertence o utilizador com o id/email recebido por parâmetro.
        
        //Em baixo segue um exemplo de instancia de Organizacao somente para teste
        return new Organizacao("Pedigree", "123456789", "987654321", "pedigree.pt", "pedi@gmail.com", new EnderecoPostal("Rua", "4455-765", "Alentejo"), new Colaborador("Maria", Constantes.PAPEL_GESTOR_ORGANIZACAO, "912345679", "maria@isep.pt"));

    }
    
        public Set<Organizacao> getListaOrganizacoes() {
        return m_lstOrganizacoes;
    }

    
    // </editor-fold>

    public Organizacao getOrganizacaoByTarefa(Tarefa tarefa) {
        //escrever metodo de busca de organizacao atraves da tarefa
        return new Organizacao();
    }
    
}
