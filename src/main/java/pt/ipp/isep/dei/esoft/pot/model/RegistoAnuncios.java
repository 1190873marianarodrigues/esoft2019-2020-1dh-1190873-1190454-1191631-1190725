/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.List;

/**
 *
 * @author user
 */
public class RegistoAnuncios {

    private final Set<Anuncio> m_lstAnuncios;
    private List<Anuncio> listaAnuncios;

    public RegistoAnuncios() {
        this.m_lstAnuncios = new HashSet<Anuncio>();
    }

    public Set<Anuncio> getListaAnunciosByTipoSeriacaoAndEmailColaborador(boolean seriacaoautomatica, String email) {
        /*escrever aqui código de procura de lista de anúncios do colaborador com o email passado por parâmetro e com um tipo de seriação automático
        ou manual, também passado por parâmetro.
         */

        return m_lstAnuncios;
    }

    public List<Anuncio> getListaAnunciosPossiveisByListaReconhecimentos(Set<Reconhecimento> listaReconhecimentos) {
        //escrever código procura lista anuncios com base nos reconhecimentos atribuídos

        return listaAnuncios;
    }

    public Anuncio getAnuncioByTarefa(Tarefa tarefa) {
        //escrever aqui código de procura de anuncio referente à tarefa passada por pârametro.

        return new Anuncio();
    }

    public List<Anuncio> getListaAnuncios() {
        return listaAnuncios;
    }

    public Anuncio getAnuncioPublicadoPor(Colaborador colab, String anuncioId) {
        //escrever aqui código de procura pelo anúncio do colaborador recebido por parâmetro que tenha o id recebido por parâmetro

        return new Anuncio();
    }

    public List<Anuncio> getAnunciosSeriaveisAutomaticos() {
        List<Anuncio> listaAnunciosCopia = listaAnuncios;
        for (int i = 0; i < listaAnunciosCopia.size(); i++) {

            Anuncio anuncio = listaAnuncios.get(i);
            TipoRegimento tipoReg = anuncio.getTipoReg();
            boolean criteriosObjetivos = tipoReg.areCriteriosObjetivos();
            if (criteriosObjetivos == false) {
                listaAnunciosCopia.remove(i);
            }
        }

        //..... continuar escrita de método
        return listaAnunciosCopia;
    }

    public Set<Anuncio> getAnunciosByColaborador(Colaborador colab) {
        //escrever aqui método que procura todos os anúncios publicados pelo colaborador recebido por parâmetro.
        
        return new HashSet<Anuncio>();
    }

    public Set<Anuncio> getAnunciosSeriaveisNaoAutomaticos(Set<Anuncio> la) {
        
        for (int i = 0; i < la.size(); i++) {

            Anuncio anuncio = listaAnuncios.get(i);
            TipoRegimento tipoReg = anuncio.getTipoReg();
            boolean criteriosObjetivos = tipoReg.areCriteriosObjetivos();
            if (criteriosObjetivos == false) {
                la.remove(i);
            }
        }

        //..... continuar escrita de método
        return la;
    }
}
