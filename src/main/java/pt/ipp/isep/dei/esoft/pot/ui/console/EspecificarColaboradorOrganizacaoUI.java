/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.ui.console;

import pt.ipp.isep.dei.esoft.pot.controller.EspecificarColaboradorOrganizacaoController;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

/**
 *
 * @author marianarodrigues163
 */
public class EspecificarColaboradorOrganizacaoUI {
    
    private EspecificarColaboradorOrganizacaoController controller;

    public EspecificarColaboradorOrganizacaoUI() {
        controller = new EspecificarColaboradorOrganizacaoController();
    }
    
    public void run() throws Exception {
        
        System.out.println("\nEspecificar Colaborador de Organização:");
        
        if(introduzDados()) {
            
            apresentaDados();
            
            if(Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if(controller.registaColaborador()) {
                    System.out.println("Especificação de colaborador efetuada com sucesso. ");
                }else{
                    System.out.println("Não foi possível concluir a especificação do colaborador com sucesso");
                }
            }
            
        }else{
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }
    
    public boolean introduzDados() throws Exception {
        String nome = Utils.readLineFromConsole("Nome do Colaborador: ");
        String funcao = Utils.readLineFromConsole("Função Desempenhada: ");
        String telefone = Utils.readLineFromConsole("Telefone: ");
        String email = Utils.readLineFromConsole("Email: ");
        
        return controller.novoColaborador(nome, funcao, telefone, email);
    }
    
    public void apresentaDados() {
        System.out.println("\n Informação a registar:\n" + controller.getColaboradorString());
    }
    
    
    
}
