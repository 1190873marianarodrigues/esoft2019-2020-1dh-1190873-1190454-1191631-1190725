/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author user
 */
public class ProcessoSeriacao {

    private Date dataHora;
    private Set<Colaborador> m_lstColaboradoresParticipantes = new HashSet<Colaborador>();
    private Set<Classificacao> m_lstClassificacoes = new HashSet<Classificacao>();
    private Colaborador colab;
    private String textoResumo;
    private TipoRegimento tipoReg;

    public ProcessoSeriacao(Colaborador colab) {
        this.colab = colab;
        this.dataHora = new Date();
    }

    public ProcessoSeriacao(TipoRegimento tipoReg) {
        this.tipoReg=tipoReg;
    }

    ProcessoSeriacao() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean novaClassificacao(Candidatura cand, int ordem, String just) {
        Classificacao classi = new Classificacao(cand, ordem, just);
        validarClassificacao(classi);
        if (validarClassificacao(classi)) {
            return addClassificacao(classi);
        } else {
            return false;
//            throw new Exception("Erro ao registar o processo de seriação");
        }
    }

    public boolean validarClassificacao(Classificacao classi) {
        //escrever código da validação do processo de seriação
        return true;
    }

    public boolean addClassificacao(Classificacao classi) {
        return m_lstClassificacoes.add(classi);
    }

    public boolean addParticipante(Colaborador colabP) {
        return m_lstColaboradoresParticipantes.add(colabP);
    }

    public void setTextoResumo(String textoResumo) {
        this.textoResumo = textoResumo;
    }

    public boolean valida(){
        //escrever código da validação do processo de atribuição tendo em conta os seus atributos.
        
        return true;
    }

    public Classificacao getPrimeiraClassificacao() {
        //escrever aqui código que retorna a primeira classificacao da lista de classificacoes.
        
        return new Classificacao();
    }

    public void ordenarByTipoRegimento(List<Candidatura> listaCand) {
        //escrever método para ordenar 
    }

    public void addClassificacao(Candidatura candi, int ordem) {
        Classificacao classi = new Classificacao(candi, ordem);
        m_lstClassificacoes.add(classi);
    }

    public Date getDataAtual() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
