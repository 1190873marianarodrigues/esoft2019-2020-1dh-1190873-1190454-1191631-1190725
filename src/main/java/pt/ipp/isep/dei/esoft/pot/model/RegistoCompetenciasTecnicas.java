/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author marianarodrigues163
 */
public class RegistoCompetenciasTecnicas {
    
    private final Set<CompetenciaTecnica> m_lstCompetencias;

    public RegistoCompetenciasTecnicas() {
       this.m_lstCompetencias = new HashSet<>();
    }
    
    public CompetenciaTecnica getCompetenciaTecnicaById(String strId)
    {
        for(CompetenciaTecnica oCompTecnica : this.m_lstCompetencias)
        {
            if (oCompTecnica.hasId(strId))
            {
                return oCompTecnica;
            }
        }
        
        return null;
    }

    public CompetenciaTecnica novaCompetenciaTecnica(String strId, String strDescricaoBreve, String strDescricaoCompleta, AreaAtividade oArea, String obrigatoriedade)
    {
        return new CompetenciaTecnica(strId, strDescricaoBreve,strDescricaoCompleta,oArea, obrigatoriedade);
    }

    public boolean registaCompetenciaTecnica(CompetenciaTecnica oCompTecnica)
    {
        if (this.validaCompetenciaTecnica(oCompTecnica))
        {
            return addCompetenciaTecnica(oCompTecnica);
        }
        return false;
    }

    private boolean addCompetenciaTecnica(CompetenciaTecnica oCompTecnica)
    {
        return m_lstCompetencias.add(oCompTecnica);
    }
    
    public boolean validaCompetenciaTecnica(CompetenciaTecnica oCompTecnica)
    {
        boolean bRet = true;
        
        // Escrever aqui o código de validação
        
        //
      
        return bRet;
    }
    
    public Set<CompetenciaTecnica> getListaCompetenciasTecnicas() {
        return m_lstCompetencias;
    }
    
}
