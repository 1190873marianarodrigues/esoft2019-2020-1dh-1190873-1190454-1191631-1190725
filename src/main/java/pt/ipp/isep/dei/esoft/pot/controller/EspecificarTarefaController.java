/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;
import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.model.CategoriaTarefa;
import pt.ipp.isep.dei.esoft.pot.model.Organizacao;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.Tarefa;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

/**
 *
 * @author user
 */
public class EspecificarTarefaController {

    private Plataforma plataforma = AplicacaoPOT.getInstance().getPlataforma();
    private Organizacao organizacao;
    private SessaoUtilizador sessao;
    private String IdCO;
    private Tarefa tarefa;
    private AutorizacaoFacade autorizacao;
    private CategoriaTarefa cat;

    public EspecificarTarefaController() {
        this.plataforma = AplicacaoPOT.getInstance().getPlataforma();
    }

    public boolean novaTarefa(String ref, String desig, String dsInf, String dsTec, String dur, String custo, String catId) throws Exception {

        try {
            this.autorizacao = this.plataforma.getAutorizacaoFacade();
            this.sessao = this.autorizacao.getSessaoAtual();
            this.IdCO = this.sessao.getIdUtilizador();
            this.organizacao = this.plataforma.getOrganizacaoByIdUtilizador(IdCO);
            this.cat = this.plataforma.getCategoriaById(catId);
            this.tarefa = this.organizacao.novaTarefa(dsTec, desig, dsInf, dsTec, dur, custo, cat);
            return this.organizacao.validaTarefa(this.tarefa);
        } catch(RuntimeException ex){
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.tarefa = null;
            return false;
        }
    }

    public boolean registaTarefa() throws Exception {


        return this.organizacao.registaTarefa(this.tarefa);
    }

    public Set<CategoriaTarefa> getListaCategoriasTarefa() {
        Set<CategoriaTarefa> lc = this.plataforma.getListaCategoriasTarefa();
        return lc;
    }

    public String getTarefaString() {
        return this.tarefa.toString();
    }
}
