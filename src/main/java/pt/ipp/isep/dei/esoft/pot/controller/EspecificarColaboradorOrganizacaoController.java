/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.autorizacao.AutorizacaoFacade;
import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.model.Colaborador;
import pt.ipp.isep.dei.esoft.pot.model.Organizacao;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

/**
 *
 * @author marianarodrigues163
 */
public class EspecificarColaboradorOrganizacaoController {

    private Plataforma plataforma;
    private Organizacao organizacao;
    private Colaborador colaborador;
    private AutorizacaoFacade autorizacao;
    private SessaoUtilizador sessao;
    private String idG;

    public EspecificarColaboradorOrganizacaoController() {
        this.plataforma = AplicacaoPOT.getInstance().getPlataforma();
    }
    
    public boolean novoColaborador(String nome, String funcao, String telefone, String email) throws Exception {
        try {
            this.colaborador = Organizacao.novoColaborador(nome, funcao, telefone, email);
            this.autorizacao = this.plataforma.getAutorizacaoFacade();
            this.sessao = this.autorizacao.getSessaoAtual();
            this.idG = this.sessao.getIdUtilizador();
            this.organizacao = this.plataforma.getOrganizacaoByIdUtilizador(idG);
            return this.organizacao.validaColaborador(colaborador);
        } catch(RuntimeException ex){
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.colaborador = null;
            return false;
        }

    }

    public boolean registaColaborador() {
        return this.organizacao.registaColaborador(colaborador);
    }
    
    public String getColaboradorString()
    {
        return this.colaborador.toString();
    }
}

