/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author user
 */
public class ProcessoAtribuicao {

    private Date dataHora;
    private Set<Colaborador> m_lstColaboradoresParticipantes = new HashSet<Colaborador>();
    private Adjudicacao adj;

    public ProcessoAtribuicao(Date dataHora) {
        this.dataHora = dataHora;
    }

    public boolean addColabordorParticipante(Colaborador colabP) {
        return m_lstColaboradoresParticipantes.add(colabP);
    }

    public boolean novaAdjudicacao(Organizacao org, Freelancer freelancer, String dsTarefa, Date dataInicioTarefa, int nrDiasNecessarios, double valorAceite, Anuncio anuncio, int numSeq, Date dataAdjudicacao) {
        Adjudicacao adj = new Adjudicacao(org,freelancer,dsTarefa, dataInicioTarefa,nrDiasNecessarios,valorAceite,anuncio,numSeq,dataAdjudicacao);
        return setAdjudicacao(adj);
    }

    private boolean setAdjudicacao(Adjudicacao adj) {
        this.adj = adj;
        return true;
    }
}
