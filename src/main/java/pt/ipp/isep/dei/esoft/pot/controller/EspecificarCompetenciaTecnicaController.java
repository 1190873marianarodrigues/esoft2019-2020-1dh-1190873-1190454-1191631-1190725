/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.pot.model.AreaAtividade;
import pt.ipp.isep.dei.esoft.pot.model.Constantes;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.CompetenciaTecnica;
import pt.ipp.isep.dei.esoft.pot.model.GrauProficiencia;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;
import pt.ipp.isep.dei.esoft.pot.model.RegistoAreasAtividade;
import pt.ipp.isep.dei.esoft.pot.model.RegistoCompetenciasTecnicas;

/**
 *
 * @author paulomaio
 */
public class EspecificarCompetenciaTecnicaController
{
    private Plataforma m_oPlataforma;
    private CompetenciaTecnica m_oCompetencia;
    private RegistoCompetenciasTecnicas rct;
    private RegistoAreasAtividade rat;
    
    public EspecificarCompetenciaTecnicaController()
    {
        if(!AplicacaoPOT.getInstance().getSessaoAtual().isLoggedInComPapel(Constantes.PAPEL_ADMINISTRATIVO))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.m_oPlataforma = AplicacaoPOT.getInstance().getPlataforma();
    }
    
    public List<AreaAtividade> getAreasAtividade()
    {
        return this.m_oPlataforma.getRegistoAreaAtividade().getAreasAtividade();
    }
    
    public boolean novaCompetenciaTecnica(String strId, String strDescricaoBreve, String strDescricaoDetalhada, String areaAtividadeId, String obrigatoriedade)
    {
        try
        {
            AreaAtividade area = this.m_oPlataforma.getRegistoAreaAtividade().getAreaAtividadeById(areaAtividadeId);
            this.m_oCompetencia = this.m_oPlataforma.getRegistoCompetenciasTecnicas().novaCompetenciaTecnica(strId, strDescricaoBreve,strDescricaoDetalhada,area,obrigatoriedade);
            
            return this.m_oPlataforma.getRegistoCompetenciasTecnicas().validaCompetenciaTecnica(this.m_oCompetencia);
        }
        catch(RuntimeException ex)
        {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.m_oCompetencia = null;
            return false;
        }
    }
    
    public boolean addGrauProficiencia(GrauProficiencia gp) {
        
        return true;
    }
   
    
    public boolean registaCompetenciaTecnica()
    {
        return this.m_oPlataforma.getRegistoCompetenciasTecnicas().registaCompetenciaTecnica(this.m_oCompetencia);
    }

    public String getCompetenciaTecnicaString()
    {
        return this.m_oCompetencia.toString();
    }
}
