/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

/**
 *
 * @author user
 */
public class Tarefa {
    
    private String referencia;
    private String designacao;
    private String descricaoInformal;
    private String descricaoTecnica;
    private String duracao;
    private String custo;
    private CategoriaTarefa categoriaTarefa;

    public Tarefa(String ref, String desig, String dsInf, String dsTec, String dur, String custo, CategoriaTarefa cat) {
        if ((ref == null) || (desig == null) || (dsInf == null) || (dsTec == null) || (dur == null) || (custo == null) || (cat == null)
        || (ref.isEmpty()) || (desig.isEmpty()) || (dsInf.isEmpty()) || (dsTec.isEmpty()) || (dur.isEmpty()) || (custo.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        
        this.referencia = ref;
        this.designacao = desig;
        this.descricaoInformal = dsInf;
        this.descricaoTecnica = dsTec;
        this.duracao = dur;
        this.custo = custo;
        this.categoriaTarefa = cat;
    }
    
    public Tarefa(){
        
    }

    @Override
    public String toString() {
        return "Tarefa{" + "referencia=" + referencia + ", designacao=" + designacao + ", descricaoInformal=" + descricaoInformal + ", descricaoTecnica=" + descricaoTecnica + ", duracao=" + duracao + ", custo=" + custo + ", categoriaTarefa=" + categoriaTarefa + '}';
    }

    public String getDescricaoTecnica() {
        return descricaoTecnica;
    }

    
    
    
    
    
}
