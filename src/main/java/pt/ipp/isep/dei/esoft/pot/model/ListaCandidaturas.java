/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.List;
import java.util.Set;

/**
 *
 * @author marianarodrigues163
 */
public class ListaCandidaturas {
    
    private Set<Candidatura> listaCandidaturas;
    private List<Candidatura> lstCandidaturas;

    public Set<Candidatura> getListaCandidaturas() {
        return listaCandidaturas;
    }
    
    public Candidatura getCandidaturaByEmailUtilizador(String email){
        //escrever aqui código de procura da candidatura através do email do utilizador responsavel pela candidatura
        
        return new Candidatura();
    }
    
    public Candidatura novaCandidatura(double valorPret, int numDiasNec, String txtMotivacao, Freelancer frl) {
        try{
        return new Candidatura(valorPret, numDiasNec, txtMotivacao, frl);
        }
        catch (Exception e){
            throw e;
        }
    }
    
    public boolean validaCandidatura(Candidatura candidatura){
        //escrever código validação candidatura
        return true;
    }
    
    public boolean registaCandidatura(Candidatura candidatura){
        if (this.validaCandidatura(candidatura)){
            return addCandidatura(candidatura);
        }
        return false;
    }

    private boolean addCandidatura(Candidatura candidatura) {
        return listaCandidaturas.add(candidatura);
    }

    public List<Candidatura> getLstCandidaturas() {
        return lstCandidaturas;
    }

    public boolean isCandidaturaInList(String idCandi) {
        //Escrever código que determina se a candidatura cujo id é recebido por parâmetro se encontra na lista de candidaturas.
        //Return true se sim, return false se não.
        return true;
    }

    public Candidatura getCandidaturaById(String idCandi) {
        //Escrever código de procura de candidatura na lista de candidaturas com o id recebido por parâmetro.
        
        return new Candidatura();
    }

    public boolean removerCandidatura(Candidatura candi) {
        //Escrever código que remove a candidatura passada por parâmetro da lista de candidauras e que retorna true se a remoção for 
        //efetuada ou false se não for.
        
        return true;
    }

    public Candidatura getCandidaturaByIdCandidatura(String candId) {
        //escrever o código que procura a candidatura com o id recebido por parâmetro
        
        return new Candidatura();
    }
    
    
    
    


}
