/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

/**
 *
 * @author marianarodrigues163
 */
public class Candidatura {
    
    private double valorPret;
    private int numDiasNec;
    private String txtMotivacao;
    private Freelancer frl;

    Candidatura(double valorPret, int numDiasNec, String txtMotivacao, Freelancer frl) {
        this.valorPret = valorPret;
        this.numDiasNec = numDiasNec;
        this.txtMotivacao = txtMotivacao;
        this.frl = frl;
    }

    public Candidatura() {
    }
    
    public boolean validaCandidatura(Candidatura candidatura){
        // escrever código para validar candidatura
        
        return true;
    }

    public boolean isCandidaturaFromFreelancer(Freelancer freel) {
        //escrever código para determinar se o freelancer recebido por parâmetro é o freelancer ao qual esta candidatura se refere.
        //Return true se sim, return false se não.
        
        return true;
    }
    
    public Freelancer getFreelancer(){
        return frl;
    }

    public double getValorPret() {
        return valorPret;
    }

    public int getNumDiasNec() {
        return numDiasNec;
    }
    
    public double setValorPretendido(double valorPrt){
        return valorPrt ;
    }
    
    public int setNumeroDias(int nrDias){
        return nrDias;
    }
    
    public String setTextoApresentacao(String txtApres){
        return txtApres;
    }
    
      public String setTextoMotivacao(String txtMotiv){
        return txtMotiv;
    }
    
    
    
}
