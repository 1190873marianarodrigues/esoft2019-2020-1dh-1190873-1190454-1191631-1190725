/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.model.Freelancer;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.RegistoFreelancers;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

/**
 *
 * @author joaom
 */
public class RegistarFreelancerController {
    
    private AplicacaoPOT app;
    private SessaoUtilizador sessao;
    private final Plataforma plataforma;
    private Freelancer freelancer;
    private String password;
    private RegistoFreelancers rf;
    
     public RegistarFreelancerController(){
        this.app = AplicacaoPOT.getInstance();
        this.plataforma = this.app.getPlataforma();
    }
     
    public boolean  novoFreelancer(String nome, String NIF, String enderecoPostal, String telefone, String email)
     {
        try
        {
            rf = this.plataforma.getRegistoFreelancer();
            return this.plataforma.validaFreelancer(this.freelancer);
        }
        catch(RuntimeException ex)
        {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.freelancer = null;
            return false;
        }
    }
    
    public boolean registaFreelancer()
    {
        return rf.registaFreelancer(this.freelancer);
    }

    public String getFreelancerString()
    {
        return this.freelancer.toString();
    }
    
    public Set <CompetenciaTecnica> getListaCompTecnicas(){
        return lstComps;
    
}

}
