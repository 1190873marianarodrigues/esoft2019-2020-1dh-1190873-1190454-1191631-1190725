/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.model.Candidatura;
import pt.ipp.isep.dei.esoft.pot.model.Freelancer;
import pt.ipp.isep.dei.esoft.pot.model.ListaCandidaturas;
import pt.ipp.isep.dei.esoft.pot.model.ListaTarefas;
import pt.ipp.isep.dei.esoft.pot.model.Organizacao;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.Reconhecimento;
import pt.ipp.isep.dei.esoft.pot.model.RegistoAnuncios;
import pt.ipp.isep.dei.esoft.pot.model.RegistoFreelancers;
import pt.ipp.isep.dei.esoft.pot.model.RegistoOrganizacoes;
import pt.ipp.isep.dei.esoft.pot.model.Tarefa;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

/**
 *
 * @author marianarodrigues163
 */
public class EfetuarCandidaturaController {

    private AplicacaoPOT m_oApp;
    private SessaoUtilizador sessao;
    private String email;
    private Plataforma plataforma;
    private Organizacao organizacao;
    private RegistoOrganizacoes ro;
    private ListaTarefas lt;
    private Anuncio anuncio;
    private RegistoAnuncios ra;
    private ListaCandidaturas lc;
    private Freelancer frl;
    private RegistoFreelancers rf;
    private Candidatura candidatura;
    private Set<Reconhecimento> m_lstReconhecimentos;
    private List<Anuncio> m_lstAnuncios;
    private Tarefa tarefa;
    private Set<Organizacao> m_lstOrganizacoes;
    private Set<Tarefa> m_lstTarefas;

    public EfetuarCandidaturaController() {
        this.m_oApp = AplicacaoPOT.getInstance();
        this.plataforma = m_oApp.getPlataforma();
    }

    public List<Anuncio> getListaAnunciosPossiveis() {
        this.sessao = m_oApp.getSessaoAtual();
        this.email = this.sessao.getEmailUtilizador();
        this.rf = this.plataforma.getRegistoFreelancers();
        this.frl = this.rf.getFreelancerByEmailUtlz(email);
        this.m_lstReconhecimentos = frl.getListaReconhecimentos();
        this.ra = this.plataforma.getRegistoAnuncios();
        this.m_lstAnuncios = ra.getListaAnunciosPossiveisByListaReconhecimentos(m_lstReconhecimentos);

        return m_lstAnuncios;
    }

    public boolean novaCandidatura(String refTarefa, double valorPret, int numDiasNec, String txtMotivacao) {

        try {
            this.ro = plataforma.getRegistoOrganizacoes();
            this.m_lstOrganizacoes = this.ro.getListaOrganizacoes();
            Tarefa tarefaReturn = null;
            int i = -1;
            do {
                while (i < m_lstOrganizacoes.size()) {
                    this.organizacao = (Organizacao) m_lstOrganizacoes.toArray()[i];

                    this.lt = this.organizacao.getListaTarefas();
                    this.m_lstTarefas = this.lt.getTarefas();
                    this.tarefa = this.lt.getTarefaByReferencia(refTarefa);
                    tarefaReturn = this.tarefa;
                }
            } while (tarefaReturn == null);
            this.anuncio = this.ra.getAnuncioByTarefa(this.tarefa);
            this.lc = this.anuncio.getListaCandidaturas();
            this.candidatura = this.lc.novaCandidatura(valorPret, numDiasNec, txtMotivacao, frl);
            return lc.validaCandidatura(this.candidatura);
        } catch (RuntimeException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.candidatura = null;
            return false;
        }

    }

    public boolean registaCandidatura(){
        return lc.registaCandidatura(this.candidatura);
    }
    
    public String getCandidaturaString() {
        return this.candidatura.toString();
    }
}
