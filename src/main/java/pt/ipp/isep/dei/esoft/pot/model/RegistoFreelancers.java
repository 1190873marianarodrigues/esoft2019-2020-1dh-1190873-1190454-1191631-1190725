/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author marianarodrigues163
 */
public class RegistoFreelancers {
    
    private Set<Freelancer> m_lstFreelancers;
    private Set<HabilitacoesAcademicas> listaHabs;
    private Set<Reconhecimento> m_lstReconhecimentos;
    private Set<CompetenciaTecnica> lstComps;
    
    public RegistoFreelancers() {
        this.m_lstFreelancers = new HashSet<Freelancer>();
        this.lstComps = new HashSet<CompetenciaTecnica>();
        
    }
    
    public Freelancer getFreelancerByEmailUtlz(String email){
        //escrever código de procura de freelancer de acordo com o email de utilizador do mesmo na plataforma
        
        return new Freelancer();
    }
    
        private Set<Freelancer> listaFreelancer = new HashSet<Freelancer>();

    public static EnderecoPostal novoEnderecoPostal(String local, String codPostal, String localidade) {
        return new EnderecoPostal(local, codPostal, localidade);
    }

    public Freelancer novoFreelancer(String nome, String NIF, String telefone, String email) {
        return new Freelancer (nome, NIF, telefone, email);

    }
    
    public HabilitacoesAcademicas novoHabilitacoesAcademicas(String grau, String curso, String instituicao, int media){
        return new HabilitacoesAcademicas(grau,curso,instituicao,media);
    }
    

    private boolean addFreelancer(Freelancer freelancer) {
        return m_lstFreelancers.add(freelancer);
    }

    public boolean validaFreelancer() {

        // Escrever aqui o código de validação
       return true;
    }
    
    public boolean validaHabilitacao(HabilitacoesAcademicas habs) {

        // Escrever aqui o código de validação
       return true;
    }
    
    private boolean addHabilitacoes(HabilitacoesAcademicas habs) {
        return listaHabs.add(habs);
    }
    
    public boolean validarReconhecimento(Reconhecimento rec) {

        // Escrever aqui o código de validação
       return true;
    }
    
     private boolean addReconhecimento(Reconhecimento rec) {
        return m_lstReconhecimentos.add(rec);
    }
    
}
