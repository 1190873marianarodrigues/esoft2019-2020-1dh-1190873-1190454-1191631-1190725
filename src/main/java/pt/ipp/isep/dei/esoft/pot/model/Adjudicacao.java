/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.model;

import java.util.Date;

/**
 *
 * @author Universidade
 */
public class Adjudicacao {
    
    private Organizacao organizacao;
    private Freelancer freelancer;
    private Date inicioPeriodoAfeto;
    private Date fimPeriodoAfeto;
    private String dsTarefa;
    private Anuncio anuncio;
    private int numSequencial;
    private Date dataAdjucacao;
    private Date dataInicioTarefa;
    private int nrDiasNecessarios;
    private double valorAceite;
    
   
    public Adjudicacao(Organizacao org, Freelancer freelancer, String dsTarefa, Date dataInicioTarefa, int nrDiasNecessarios, double valorAceite, Anuncio anuncio, int numSeq, Date dataAdjudicacao) {
        this.organizacao = org;
        this.freelancer = freelancer;
        this.dsTarefa = dsTarefa;
        this.dataInicioTarefa = dataInicioTarefa;
        this.nrDiasNecessarios = nrDiasNecessarios;
        this.valorAceite = valorAceite;
        this.anuncio = anuncio;
        this.numSequencial = numSeq;
        this.dataAdjucacao = dataAdjudicacao;
    }
    
     Adjudicacao() {
        
    }

    
}






