/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.ui.console;

import pt.ipp.isep.dei.esoft.pot.controller.EspecificarTarefaController;
import pt.ipp.isep.dei.esoft.pot.controller.RegistarOrganizacaoController;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

/**
 *
 * @author paulomaio
 */
public class EspecificarTarefaUI
{
    private EspecificarTarefaController m_controller;
    public EspecificarTarefaUI()
    {
        m_controller = new EspecificarTarefaController();
    }

    public void run() throws Exception
    {
        System.out.println("\nEspecificarTarefa:");

        if(introduzDados())
        {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.registaTarefa()) {
                    System.out.println("Especificacao efetuada com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir a especificacao com sucesso.");
                }
            }
        }
        else
        {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }
    
    private boolean introduzDados() throws Exception {
        String strRef = Utils.readLineFromConsole("Referencia da Tarefa: ");
        String strDes = Utils.readLineFromConsole("Designação da Tarefa: ");
        String strDsInf = Utils.readLineFromConsole("Descricao informal: ");
        String strDsTec = Utils.readLineFromConsole("Descricao Tecnica: ");
        String strDur = Utils.readLineFromConsole("Duracao: ");
        String strCusto = Utils.readLineFromConsole("Custo: ");
        String strCatId = Utils.readLineFromConsole("Id da categoria em que se enquadra: ");
       
        
        return m_controller.novaTarefa(strRef,strDes,strDsInf,strDsTec,strDur,strCusto,strCatId);
    }
    
    private void apresentaDados() 
    {
        System.out.println("\n Informação a Registar:\n" + m_controller.getTarefaString());
    }
}
