/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.ui.console;

import java.util.List;
import java.util.Set;
import pt.ipp.isep.dei.esoft.pot.controller.EfetuarCandidaturaController;
import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

/**
 *
 * @author marianarodrigues163
 */
public class EfetuarCandidaturaUI {
    
    private EfetuarCandidaturaController m_controller;

    public EfetuarCandidaturaUI() {
        m_controller = new EfetuarCandidaturaController();
    }
    
    public void run()
    {
        System.out.println("\nEfetuar Candidatura:");

        if(introduzDados())
        {
            apresentaDados();

            if (Utils.confirma("Confirma os dados introduzidos? (S/N)")) {
                if (m_controller.registaCandidatura()) {
                    System.out.println("Registo efetuado com sucesso.");
                } else {
                    System.out.println("Não foi possivel concluir o registo com sucesso.");
                }
            }
        }
        else
        {
            System.out.println("Ocorreu um erro. Operação cancelada.");
        }
    }
    
    private boolean introduzDados() {
        List<Anuncio> la = m_controller.getListaAnunciosPossiveis();
        Anuncio anuncio = (Anuncio) Utils.apresentaESeleciona(la, "Selecione o anúncio ao qual se deseja candidatar:");
        Double doubValorPretendido = Utils.readDoubleFromConsole("Valor pretendido pela realização da tarefa: ");
        int intNumDiasNecessarios = Utils.readIntegerFromConsole("Número de dias necessários para a realização da tarefa: ");
        String strTextoMotivacao = Utils.readLineFromConsole("Texto de apresentação/motivação: (facultativo) ");
        String refTarefa = "";
        if (strTextoMotivacao == null){
            strTextoMotivacao = "";
        }
        return m_controller.novaCandidatura(refTarefa, doubValorPretendido, intNumDiasNecessarios, strTextoMotivacao);
    }
    
    private void apresentaDados(){
        System.out.println("\nCandidatura: \n" + m_controller.getCandidaturaString());
    }
    
    
    
}
