/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.Date;
import java.util.Set;
import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.model.Candidatura;
import pt.ipp.isep.dei.esoft.pot.model.Classificacao;
import pt.ipp.isep.dei.esoft.pot.model.Colaborador;
import pt.ipp.isep.dei.esoft.pot.model.ListaCandidaturas;
import pt.ipp.isep.dei.esoft.pot.model.ListaTarefas;
import pt.ipp.isep.dei.esoft.pot.model.Organizacao;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.ProcessoSeriacao;
import pt.ipp.isep.dei.esoft.pot.model.RegistoAnuncios;
import pt.ipp.isep.dei.esoft.pot.model.RegistoOrganizacoes;
import pt.ipp.isep.dei.esoft.pot.model.Tarefa;

/**
 *
 * @author Universidade
 */
public class AdjudicarTarefaControlller {
    private AplicacaoPOT app;
    private SessaoUtilizador sessao;
    private String email;
    private RegistoAnuncios ra;
    private Set<Anuncio> la;
    private RegistoOrganizacoes ro;
    private Organizacao org;
    private ListaTarefas lt;
    private Tarefa tarefa;
    private Anuncio anuncio;
    private ProcessoAtribuicao processoA;
    private ListaCandidaturas lc;
    private Plataforma plataforma;
    
    public AdjudicarTarefaControlller(){
        this.app = AplicacaoPOT.getInstance();
        this.plataforma = this.app.getPlataforma();
    }
    
    public Set<Anuncio> getListaAnuncios(){
        
        this.sessao = this.app.getSessaoAtual();
        this.email = this.sessao.getEmailUtilizador();
        this.ra = this.plataforma.getRegistoAnuncios();
        this.la = ra.getListaAnunciosByTipoSeriacaoAndEmailColaborador(false, this.email);
        return this.la;
    }
    
    public void novoProcessoAtribuicao(String refTarefa, Date dataHora){
        
        this.ro = this.plataforma.getRegistoOrganizacoes();
        this.org = this.ro.getOrganizacaoByIdUtilizador(this.email);
        this.lt = this.org.getListaTarefas();
        this.tarefa = lt.getTarefaByReferencia(refTarefa);
        this.anuncio = this.ra.getAnuncioByTarefa(this.tarefa);
        this.processoA = this.anuncio.novoProcessoAtribuicao(dataHora);
    }
 
    public Set<Colaborador> getListaColaboradores(){
        return this.org.getListaColaboradores();
    }
    
    
    public boolean addColaboradorParticipante(String emailColabP){
        Colaborador colabP = this.org.getColaboradorByEmailUtilizador(email);
        return this.processoA.addColabordorParticipante(colabP);
    }
    
    public Set<Candidatura> getListaCandidaturas(){
        this.lc = this.anuncio.getListaCandidaturas();
        return this.lc.getListaCandidaturas();
    }
    
    public boolean registaProcessoAtribuicao(){
        return this.anuncio.registarProcessoAtribuicao(this.processoA);
    }
}

