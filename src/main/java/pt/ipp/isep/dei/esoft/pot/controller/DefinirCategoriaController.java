/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.esoft.pot.model.AreaAtividade;
import pt.ipp.isep.dei.esoft.pot.model.CategoriaTarefa;
import pt.ipp.isep.dei.esoft.pot.model.CompetenciaTecnica;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.ui.console.utils.Utils;

/**
 *
 * @author joaom
 */
public class DefinirCategoriaController {

    private Plataforma plataforma = AplicacaoPOT.getInstance().getPlataforma();
    private CategoriaTarefa categoria;
    private AreaAtividade areaAtividade;
    private Set<CompetenciaTecnica> compTecnicas = new HashSet<CompetenciaTecnica>() ;

    public DefinirCategoriaController() {
        this.plataforma = AplicacaoPOT.getInstance().getPlataforma();
    }


    public boolean novaCategoriaTarefa(String identInterno, String descriçao, String areaAtividadeId, ArrayList <String> compTecnicasId) {
        try {
            this.areaAtividade = this.plataforma.getAreaAtividadeByID(areaAtividadeId);
            for(int i =0;i<compTecnicasId.size();i++){
                this.compTecnicas.add(this.plataforma.getCompetenciaTecnicaById2(compTecnicasId.get(i)));
            }
            this.categoria = this.areaAtividade.novaCategoriaTarefa(identInterno, descriçao, areaAtividade, compTecnicas);
            return this.areaAtividade.validaCategoriaTarefa(this.categoria);
            
        } catch (RuntimeException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.categoria = null;
            return false;
        }
    }

    public boolean registaCategoriaTarefa() {
        return this.areaAtividade.registaCategoriaTarefa(this.categoria);
    }
    
    public Set<AreaAtividade> mostrarListaAreasAtividade(){
        Set<AreaAtividade> lc = (Set<AreaAtividade>) this.plataforma.getAreasAtividade();
        return lc;
    }
    
    public Set<CompetenciaTecnica> mostrarListaCompTecnicas(){
        Set<CompetenciaTecnica> lc = this.plataforma.getListaCompetenciasTecnicas();
        return lc;
    }
    
    public String getCategoriaTarefaString() {
        return this.categoria.toString();
    }
}