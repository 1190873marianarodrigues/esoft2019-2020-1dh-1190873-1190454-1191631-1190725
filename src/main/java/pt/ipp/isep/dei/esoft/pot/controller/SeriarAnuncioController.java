/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.Date;
import java.util.Set;
import pt.ipp.isep.dei.esoft.autorizacao.model.SessaoUtilizador;
import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.model.Candidatura;
import pt.ipp.isep.dei.esoft.pot.model.Classificacao;
import pt.ipp.isep.dei.esoft.pot.model.Colaborador;
import pt.ipp.isep.dei.esoft.pot.model.Freelancer;
import pt.ipp.isep.dei.esoft.pot.model.ListaCandidaturas;
import pt.ipp.isep.dei.esoft.pot.model.ListaColaboradores;
import pt.ipp.isep.dei.esoft.pot.model.ListaTarefas;
import pt.ipp.isep.dei.esoft.pot.model.Organizacao;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.ProcessoAtribuicao;
import pt.ipp.isep.dei.esoft.pot.model.ProcessoSeriacao;
import pt.ipp.isep.dei.esoft.pot.model.RegistoAnuncios;
import pt.ipp.isep.dei.esoft.pot.model.RegistoOrganizacoes;
import pt.ipp.isep.dei.esoft.pot.model.Tarefa;
import pt.ipp.isep.dei.esoft.pot.model.TipoRegimento;

/**
 *
 * @author user
 */
public class SeriarAnuncioController {

    private AplicacaoPOT app;
    private SessaoUtilizador sessao;
    private String email;
    private RegistoAnuncios ra;

    private RegistoOrganizacoes ro;
    private Organizacao org;
    private ListaTarefas lt;
    private Tarefa tarefa;
    private Anuncio anuncio;
    private ProcessoSeriacao processoS;
    private ListaCandidaturas lc;
    private final Plataforma plataforma;
    private boolean tipoSeriacao;
    private ProcessoAtribuicao processoA;

    public SeriarAnuncioController() {
        this.app = AplicacaoPOT.getInstance();
        this.plataforma = this.app.getPlataforma();
        this.ra = this.plataforma.getRegistoAnuncios();
        this.ro = this.plataforma.getRegistoOrganizacoes();
        this.sessao = this.app.getSessaoAtual();
        this.email = this.sessao.getEmailUtilizador();
    }

    public Set<Anuncio> getAnunciosPorSeriarNaoAutomaticos() {
        Organizacao org = ro.getOrganizacaoByIdUtilizador(this.email);
        ListaColaboradores lstColabs = org.obterListaColaboradores();
        Colaborador colab = lstColabs.getColaboradorByEmail(email);
        Set<Anuncio> la = this.ra.getAnunciosByColaborador(colab);
        la = this.ra.getAnunciosSeriaveisNaoAutomaticos(la);
        return la;
    }

    public Set<Candidatura> getCandidaturas(String anuncioId) {
        RegistoAnuncios ra = this.plataforma.getRegistoAnuncios();
        Organizacao org = ro.getOrganizacaoByIdUtilizador(this.email);
        ListaColaboradores lstColabs = org.obterListaColaboradores();
        Colaborador colab = lstColabs.getColaboradorByEmail(email);
        Anuncio anuncio = ra.getAnuncioPublicadoPor(colab, anuncioId);
        ListaCandidaturas listaCand = this.anuncio.getListaCandidaturas();
        Set<Candidatura> lstCand = listaCand.getListaCandidaturas();
        this.processoS = anuncio.novoProcessoSeriacao(colab);
        return lstCand;
    }

    public boolean classifica(String candId, int ordem, String just) {
        Candidatura cand = this.lc.getCandidaturaByIdCandidatura(candId);
        return this.processoS.novaClassificacao(cand, ordem, just);
    }

    public Set<Colaborador> getColaboradores() {
        Organizacao org = this.ro.getOrganizacaoByIdUtilizador(this.email);
        ListaColaboradores lstColabs = org.obterListaColaboradores();
        Set<Colaborador> lColabs = lstColabs.getListaColaboradores();
        return lColabs;
    }

    public boolean addParticipante(String emailPart) {
        Organizacao org = this.ro.getOrganizacaoByIdUtilizador(this.email);
        ListaColaboradores lstColabs = org.obterListaColaboradores();
        Colaborador colabP = lstColabs.getColaboradorByEmail(email);
        return this.processoS.addParticipante(colabP);
    }

    public void setTextoResumo(String txtResumo) {
        this.processoS.setTextoResumo(txtResumo);
    }

    public boolean registaProcessoSeriacaoEVerificaObrigatoriedade() {
        this.anuncio.registarProcessoSeriacao(this.processoS);
        TipoRegimento tipoReg = this.anuncio.getTipoReg();
        return tipoReg.getObrigatoriedade();
    }
    
    public boolean novoProcessoAtribuicao(){
        Date dataAtribuicao = getDataAtual();
        this.processoA = this.anuncio.novoProcessoAtribuicao(dataAtribuicao);
        Classificacao primeiraClassificacao = this.processoS.getPrimeiraClassificacao();
        Candidatura candi = primeiraClassificacao.getCandidatura();
        Freelancer freelancer = candi.getFreelancer();
        Tarefa tarefa = this.anuncio.getTarefa();
        String dsTarefa = tarefa.getDescricaoTecnica();
        double valorAceite = candi.getValorPret();
        int nrDias = candi.getNumDiasNec();
        Date dataInicioTarefa = getDataDiaSeguinte();
        Date dataAdjudicacao = getDataAtual();
        int numSeq = this.plataforma.getNumeroSequencial();
        return this.processoA.novaAdjudicacao(this.org, freelancer, dsTarefa, dataInicioTarefa, nrDias, valorAceite,
                this.anuncio, numSeq, dataAdjudicacao);
    }
    
    private Date getDataDiaSeguinte() {
        //Escrever código para que o método retorne a data do dia seguinte ao atual
        
        return new Date();
    }

    private Date getDataAtual() {
       //Escrever código para que o método retorne a data do dia atual
       
       return new Date();
    }

}
