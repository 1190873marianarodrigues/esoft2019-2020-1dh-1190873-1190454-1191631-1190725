/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ipp.isep.dei.esoft.pot.controller;

import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import pt.ipp.isep.dei.esoft.pot.model.Adjudicacao;
import pt.ipp.isep.dei.esoft.pot.model.Anuncio;
import pt.ipp.isep.dei.esoft.pot.model.Candidatura;
import pt.ipp.isep.dei.esoft.pot.model.Classificacao;
import pt.ipp.isep.dei.esoft.pot.model.Freelancer;
import pt.ipp.isep.dei.esoft.pot.model.ListaCandidaturas;
import pt.ipp.isep.dei.esoft.pot.model.Organizacao;
import pt.ipp.isep.dei.esoft.pot.model.Plataforma;
import pt.ipp.isep.dei.esoft.pot.model.ProcessoAtribuicao;
import pt.ipp.isep.dei.esoft.pot.model.ProcessoSeriacao;
import pt.ipp.isep.dei.esoft.pot.model.RegistoAnuncios;
import pt.ipp.isep.dei.esoft.pot.model.RegistoOrganizacoes;
import pt.ipp.isep.dei.esoft.pot.model.Tarefa;
import pt.ipp.isep.dei.esoft.pot.model.TipoRegimento;

/**
 *
 * @author marianarodrigues163
 */
public class SeriarAnuncioCandidaturaTask extends TimerTask {

    Timer timer = new Timer();
    TimerTask task = new SeriarAnuncioCandidaturaTask();

    private AplicacaoPOT app;
    private RegistoAnuncios ra;
    private Anuncio anuncio;
    private ProcessoSeriacao processoS;
    private ListaCandidaturas lc;
    private Plataforma plataforma;
    private boolean tipoSeriacao;
    private ProcessoAtribuicao processoA;
    private List<Anuncio> listaAnunciosCopia;
    private List<Candidatura> listaCandidaturas;
    private TipoRegimento tipoReg;
    private RegistoOrganizacoes ro;

    @Override
    public void run() {
        seriarCandidaturasSubmetidas();
    }

    public SeriarAnuncioCandidaturaTask() {
        this.app = AplicacaoPOT.getInstance();
        this.plataforma = this.app.getPlataforma();

    }

    private void seriarCandidaturasSubmetidas() {
        this.ra = this.plataforma.getRegistoAnuncios();
        this.listaAnunciosCopia = ra.getAnunciosSeriaveisAutomaticos();
        for (int i = 0; i < listaAnunciosCopia.size(); i++) {
            this.anuncio = listaAnunciosCopia.get(i);
            this.lc = anuncio.getListaCandidaturas();
            this.listaCandidaturas = lc.getLstCandidaturas();
            this.processoS = anuncio.novoProcessoSeriacao();
            processoS.ordenarByTipoRegimento(listaCandidaturas);
            for (int j = 0; j < listaCandidaturas.size(); j++) {
                Candidatura candi = listaCandidaturas.get(j);
                processoS.addClassificacao(candi, j);
            }
            Date dataAtual = processoS.getDataAtual();
            anuncio.registarProcessoSeriacao(processoS);
        }
        boolean obrigatoriedade = anuncio.getTipoReg().getObrigatoriedade();
        if (obrigatoriedade = true) {
            processoA = anuncio.novoProcessoAtribuicao();
            Date dataAtribuicao = getDataAtual();
            this.processoA = this.anuncio.novoProcessoAtribuicao(dataAtribuicao);
            Classificacao primeiraClassificacao = this.processoS.getPrimeiraClassificacao();
            Candidatura candi = primeiraClassificacao.getCandidatura();
            Freelancer freelancer = candi.getFreelancer();
            Tarefa tarefa = this.anuncio.getTarefa();
            this.ro = this.plataforma.getRegistoOrganizacoes();
            Organizacao org = ro.getOrganizacaoByTarefa(tarefa);
            String dsTarefa = tarefa.getDescricaoTecnica();
            double valorAceite = candi.getValorPret();
            int nrDias = candi.getNumDiasNec();
            Date dataInicioTarefa = getDataDiaSeguinte();
            Date dataAdjudicacao = getDataAtual();
            int numSeq = this.plataforma.getNumeroSequencial();
            processoA.novaAdjudicacao(org, freelancer, dsTarefa, dataInicioTarefa, nrDias, valorAceite, anuncio, numSeq, dataAdjudicacao);
            
        }
    }

    private Date getDataAtual() {
        //escrever método de obtençao de data atual
        
        return new Date();
    }

    private Date getDataDiaSeguinte() {
        //escrever método data dia seguinte ao atual
        
        return new Date();
    }

}
