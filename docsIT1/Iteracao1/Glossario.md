# Glossário

**Os termos devem estar organizados alfabeticamente.**

(Completar)

| **_Termo_**                   	| **_Descrição_**                                           |                                       
|:------------------------|:----------------------------------------------------------------|
| **Administrativo** | Pessoa responsável por realizar na plataforma várias atividades de suporte ao negócio.|
| **ADM** | Acrónimo para Administrativo.|
| **Competência Técnica** | Competência requerida para a realização de uma tarefa referente a uma área de atividade. |
| **Caracter Competência** | Caracter referente a uma competência técnica que pode ser obrigatória ou desejável.|
| **Categoria (de Tarefa)** | Corresponde a uma descrição usada para catalogar um ou mais tarefas (semelhantes).|
| **Colaborador de Organização** | Pessoa pertencente a uma organização responsável por especificar as tarefas, entre outras responsabilidades. |
| **_Freelancer_** | Pessoa individual que trabalha por conta própria e não está necessariamente comprometida com uma entidade empregadora (organização) específica a longo prazo.|
| **Gestor de Organização** | Pessoa pertencente a uma organização, responsável por especificar outros colaboradores dessa mesma organização. |
| **Organização** | Pessoa coletiva que pretende contratar _freelancers_ para a realização de tarefas necessárias à atividade da mesma.|
| **Processo de Autenticação** | Meio através do qual se procede à verificação da identidade da pessoa que pretende/está a utilizar a plataforma informática.|
| **Tarefa** | Corresponde ao objeto de negócio proposto para realização por parte dos _freelancers_. |
| **Utilizador** | Pessoa que interage com a aplicação informática.|
| **Utilizador Não Registado** | Utilizador que interage com a plataforma informática de forma anónima, i.e. sem ter realizado previamente o processo de autenticação previsto.|
| **Utilizador Registado** | Utilizador que interage com a plataforma informática após ter realizado o processo de autenticação previsto e, portanto, a aplicação conhece a sua identidade. Tipicamente, este assume o papel/função de Administrativo ou Gestor de Organização ou Colaborador de Organização ou _Freelancer_.|
