# UC4- Especificar Competência Técnica

## 1. Engenharia de Requisitos

### Formato Breve

O administrativo inicia a definição de uma especificação técnica, solicita os dados necessários (i.e. através	 de	 um	 código	 único,	 uma	 descrição	 breve,	 outra	 mais	 detalha, área de atividade e seu caracter de competência). O administrativo introduz os dados solicitados. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. O administrativo confirma. O sistema regista os dados e informa o administrativo do sucesso da operação.

### SSD
![UC4_SSD.svg](UC4_SSD.svg)


### Formato Completo

#### Ator principal

Administrativo

#### Partes interessadas e seus interesses
* **Administrativo:** pretende especificar competências técnicas requeridas para a realização de tarefas em área de atividade.
* **T4J:** pretende que a plataforma permita catalogar as competências técnicas em áreas de atividade.


#### Pré-condições
n/a

#### Pós-condições
Toda a informação da especificação técnica é registada no sistema.

### Cenário de sucesso principal (ou fluxo básico)

1. O administrativo inicia a especificação de uma nova competência técnica.
2. O sistema solicita os dados necessários (i.e. código único, descrição breve, descrição detalhada).
3. O administrativo introduz os dados solicitados.
4. O Sistema apresenta uma listagem das áreas de atividade já registadas.
5. O administrativo seleciona uma área de atividade.
6. O sistema solicita carater de competência.
7. O administrativo introduz carater de competência.
8. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.
9. O administrativo confirma.
10. O sistema regista os dados e informa o administrativo do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O administrativo solicita o cancelamento da definição da especificação de competência técnica.

> O caso de uso termina.

4a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O administrativo não altera os dados. O caso de uso termina.

4b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o administrativo para o facto.
>	2. O sistema permite a sua alteração (passo 3)
>
	>	2a. O administrativo não altera os dados. O caso de uso termina.

4c. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o administrativo para o facto.
> 2. O sistema permite a sua alteração (passo 3).
>
	> 2a. O administrativo não altera os dados. O caso de uso termina.

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Existem outras especificações  de competências técnicas que são necessários?
* Todas as especificações técnicas são obrigatórios?
* O código único é sempre introduzido pelo administrativo ou o sistema deve gerá-lo automaticamente?
* Qual a frequência de ocorrência deste caso de uso?


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC4_MD.svg](UC4_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O administrativo inicia a definição de uma nova especificação de competência técnica.   		 |	... interage com o utilizador? | DefinirAreaUI    |  Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio. |
|  		 |	... coordena o UC?	| DefinirAreaController | Controller    |
|  		 |	... cria instância de Especificação de Competência Técnica | Plataforma   | Creator (Regra1)   |
| 2. O sistema solicita os dados necessários (i.e. código único, descrição breve, detalhada).  		 |							 |             |                              |
| 3. O administrativo introduz os dados solicitados.  		 |	... guarda os dados introduzidos?  |   Competência Técnica | Information Expert (IE) - instância criada no passo 1     |
| 4. O sistema solicita ao colaborador o carater de obrigatoriedade. | | | |
| 5. O administrativo introduz o carater de obrigatoriedade.  		 |	... guarda o caracter de obrigatoriedade  |   Competência Técnica | Information Expert (IE) - instância criada no passo 1     |
| 6. O sistema apresenta ao colaborador de organização a lista de áreas de atividade. | | | |
| 7. O colaborador seleciona as áreas de atividade.   		 |	...guarda a área de	atividade					 |     Competência Técnica        |           Information Expert (IE) - instância criada no passo 1                   |
| 8. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.   		 |	...valida os dados da Competência Técnica (validação local) | Competência Técnica |                              |IE. Possui os seu próprios dados.|  	
|	 |	...valida os dados da Competência Técnica (validação global) | Plataforma  | IE: A Plataforma possui/agrega Competência Técnica  |
| 9. O administrativo confirma.   		 |							 |             |                              |
| 10. O sistema regista os dados e informa o administrativo do sucesso da operação.  		 |	... guarda a Competência Técnica criada? | Plataforma  | IE: No MD a Plataforma possui Especificação da Competência Técnica|  


### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Administrativo
 * Competência Técnica
 * Área Atividade


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * EspecificarCompetênciaTécnicaUI  
 * EspecificarCompetênciaTécnicaController


###	Diagrama de Sequência

![UC4_SD.svg](UC4_SD.svg)


###	Diagrama de Classes

![UC4_CD.svg](UC4_CD.svg)
