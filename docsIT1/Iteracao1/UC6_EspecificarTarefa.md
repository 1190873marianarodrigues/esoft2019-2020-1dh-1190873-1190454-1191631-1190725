# UC6 - Especificar Tarefa

## 1. Engenharia de Requisitos

### Formato Breve

O colaborador de organização inicia a especificação da tarefa. O sistema solicita os dados necessários sobre a tarefa (i.e. referência, designação, descrição informal, descrição de caráter técnico, duração, custo, categoria) A categoria é selecionada a partir das categorias já pré-definidas pelo administrador. O colaborador de organização introduz os dados requeridos. O sistema valida e apresenta os dados, pedindo que os confirme. O colaborador confirma. O sistema regista a tarefa especificada tornando e informa o colaborador do sucesso da operação.

### SSD
![UC6_SSD.svg](UC6_SSD.svg)


### Formato Completo

#### Ator principal

Colaborador de Organização

#### Partes interessadas e seus interesses
* **Colaborador de Organização:** pretende especificar as tarefas da sua organização com o intuito de esclarecer os possíveis interessadas (freelancers).
* **Freelancers:** pretende que o colaborador da organização especifique a tarefa de modo a obter conhecimento dos detalhes da mesma.


#### Pré-condições
n/a

#### Pós-condições
A tarefa é registada no sistema.

### Cenário de sucesso principal (ou fluxo básico)

1. O colaborador de organização inicia a especificação da tarefa.  
2. O sistema solicita os dados necessários (i.e. referência, designação, descrição informal, descrição de caráter técnico, duração, custo).
3. O colaborador de organização introduz os dados requeridos.
4. O sistema apresenta ao colaborador de organização a lista de categorias de tarefa.
5. O colaborador seleciona a categoria de tarefa.
6. O sistema valida e apresenta os dados ao colaborador de organização, pedindo que os confirme.
7. O colaborador de organização confirma.
8. O sistema regista os dados e informa o colaborador de organização do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O colaborador de organização solicita o cancelamento da definição da especificação de tarefa.

> O caso de uso termina.

6a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O colaborador de organização não altera os dados. O caso de uso termina.

6b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o colaborador de organização para o facto.
>	2. O sistema permite a sua alteração (passo 3)
>
	>	2a. O colaborador de organização não altera os dados. O caso de uso termina.

6c. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o colaborador de organização para o facto.
> 2. O sistema permite a sua alteração (passo 3).
>
	> 2a. O colaborador de organização não altera os dados. O caso de uso termina.

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Existem outros dados que são necessários?
* Todos os dados são obrigatórios?
* As referências associam-se à tarefa no geral, ou a uma tarefa de uma empresa específica?
* Qual a frequência de ocorrência deste caso de uso?


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC6_MD.svg](UC6_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O colaborador de organização inicia a especificação da tarefa.   		 |	... interage com o utilizador? | EspecificarTarefaUI    |  Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio. |
|  		 |	... coordena o UC?	| EspecificarTarefaController | Controller    |
|  		 |	... cria instância de Tarefa| Organizacao   | Creator (Regra1)   |
| 2. O sistema solicita os dados necessários (i.e. referência, designação, descrição informal, descrição de caráter técnico, duração, custo).  		 |							 |             |                              |
| 3. O colaborador de organização introduz os dados requeridos.  		 |	... guarda os dados introduzidos?  |   Tarefa | Information Expert (IE) - instância criada no passo 1     |
| 4. O sistema apresenta ao colaborador de organização a lista de categorias de tarefa. | | | |
| 5. O colaborador seleciona a categoria de tarefa.   		 |	...guarda a categoria de tarefa						 |     Tarefa        |           Information Expert (IE) - instância criada no passo 1                   |
| 6. O sistema valida e apresenta os dados ao colaborador de organização, pedindo que os confirme.  		 |	...valida os dados da Tarefa (validação local) | Tarefa  | A classe Tarefa responsabiliza-se pelas instâncias criadas |
|  |  ...valida os dados da Tarefa (validação global) | Organizacao | IE: A Organizacao possui/agrega Tarefa |
| 7. O colaborador de organização confirma. |         |           |            |
| 8. O sistema regista os dados e informa o colaborador de organização do sucesso da operação.  |    ... guarda a Tarefa criada?      |    Organizacao         |      IE: No MD a Organizacao possui Tarefa      |



### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Organizacao
 * CategoriaTarefa
 * Tarefa
 * SessaoUtilizador
 * AutorizacaoFacade


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * EspecificarTarefaUI  
 * EspecificarTarefaController


###	Diagrama de Sequência

![UC6_SD.svg](UC6_SD.svg)


###	Diagrama de Classes

![UC6_CD.svg](UC6_CD.svg)
