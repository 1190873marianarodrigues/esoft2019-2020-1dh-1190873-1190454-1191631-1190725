# UC3 - Definir Categoria (De Tarefa)

## 1. Engenharia de Requisitos

### Formato Breve

O administrativo define as categorias de tarefas. O sistema solicita os dados necessários (i.e. identificador interno, descrição, área de atividade(apenas uma), competências técnicas). O administrativo introduz os dados solicitados. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. O administrativo confirma. O sistema regista os dados e informa o administrativo do sucesso da operação.

### SSD
![UC3_SSD.svg](UC3_SSD.svg)


### Formato Completo

#### Ator principal

Administrativo

#### Partes interessadas e seus interesses
* **Administrativo:** pretende definir as categorias de tarefas para posteriormente especificar as competências técnicas requeridas para a realização das mesmas.
* **T4J:** pretende que a plataforma permita catalogar as competências técnicas e as categorias de tarefas em áreas de atividade.


#### Pré-condições
n/a

#### Pós-condições
A informação da categoria de tarefa é registada no sistema.

### Cenário de sucesso principal (ou fluxo básico)

1. O administrativo inicia a definição de uma nova categoria de tarefa. 
2. O sistema solicita os dados necessários (i.e. identificador interno, descrição, área de atividade, competências técnicas).
3. O administrativo introduz os dados solicitados.
4. O sistema apresenta uma listagem de áreas de atividade já registadas.
5. O administrativo seleciona uma área de atividade. 
6. O sistema apresenta uma listagem de competências técnicas já resgistadas.
7. O administrativo seleciona as competências técnicas necessárias.
8. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme. 
9. O administrativo confirma. 
10. O sistema regista os dados e informa o administrativo do sucesso da operação.


#### Extensões (ou fluxos alternativos)

*a. O administrativo solicita o cancelamento da definição da  categoria de tarefa.

> O caso de uso termina.

4a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O administrativo não altera os dados. O caso de uso termina.

4b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o administrativo para o facto.
>	2. O sistema permite a sua alteração (passo 3)
>
	>	2a. O administrativo não altera os dados. O caso de uso termina.

4c. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o administrativo para o facto. 
> 2. O sistema permite a sua alteração (passo 3).
> 
	> 2a. O administrativo não altera os dados. O caso de uso termina. 

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Existem outros dados que são necessários?
* Todos os dados são obrigatórios?
* O código único é sempre introduzido pelo administrativo ou o sistema deve gerá-lo automaticamente?
* Qual a frequência de ocorrência deste caso de uso?


## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC3_MD.svg](UC3_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O administrativo inicia a definição de uma nova categoria de tarefa.   		 |	... interage com o utilizador? | DefinirCategoriaUI    |  Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio. |
|  		 |	... coordena o UC?	| DefinirCategoriaController | Controller    |
|  		 |	... cria instância de CategoriaTarefa| Plataforma   | Creator (Regra1)   |
| 2. O sistema solicita os dados necessários (i.e. identificador interno, descrição, área de atividade, competências técnicas).  		 |							 |             |                              |
| 3. O administrativo introduz os dados solicitados.  		 |	... guarda os dados introduzidos?  |   CategoriaTarefa | Information Expert (IE) - instância criada no passo 1     |
| 4. O sistema apresenta uma listagem de áreas de atividade já registadas. 
| 5. O administrativo seleciona uma área de atividade.  |  ...guarda a área de atividade?  |  CategoriaTarefa |  Information Expert (IE) - instância criada no passo 1
| 6. O sistema apresenta uma listagem de competências técnicas já registadas.
| 7. O administrativo seleciona as competências técnicas necessárias.  |  ...guarda as competências técnicas?  |  CategoriaTarefa  |  Information Expert (IE) - instância criada no passo 1    
| 8. O sistema valida e apresenta os dados ao administrativo, pedindo que os confirme.   		 |	...valida os dados da CategoriaTarefa (validação local) | CategoriaTarefa  |  A classe CategoriaTarefa responsabiliza-se pelas instâncias criadas                            |IE. Possui os seu próprios dados.|  	
|	 |	...valida os dados da CategoriaTarefa (validação global) | AreaDeAtividade  | IE: A AreaDeAtividade possui/agrega CategoriaTarefa  |
| 9. O administrativo confirma.   		 |							 |             |                              |
| 10. O sistema regista os dados e informa o administrativo do sucesso da operação.  		 |	... guarda a CategoriaTarefa criada? | Plataforma  | IE: No MD a Plataforma possui AreaAtividade|  
             

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * CategoriaTarefa
 * AreaAtividade


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * DefinirCategoriaUI  
 * DefinirCategoriaController


###	Diagrama de Sequência

![UC3_SD.svg](UC3_SD.svg)


###	Diagrama de Classes

![UC3_CD.svg](UC3_CD.svg)




