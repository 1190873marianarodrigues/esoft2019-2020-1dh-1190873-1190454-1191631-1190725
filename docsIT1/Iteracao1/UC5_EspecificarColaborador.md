# UC5 - Especificar  Colaborador de Organização

## 1. Engenharia de Requisitos

### Formato Breve

O gestor de organização inicia a especificação de um colaborador da sua organização. O sistema solicita os dados necessários sobre o colaborador (i.e. nome, função, contacto telefónico, email). O gestor de organização introduz introduz os dados solicitados. O sistema valida e apresenta os dados, pedindo que os confirme. O gestor de organização confirma. O sistema **regista os dados do colaborador, tornando este último um utilizador registado** e informa o utilizados não registado do sucesso da operação.

### SSD
![UC5_SSD.svg](UC5_SSD.svg)


### Formato Completo

#### Ator principal

Gestor de Colaboração

#### Partes interessadas e seus interesses
* **Gestor de organização:** pretende especificar os colaboradores que pertencem à sua mesma organização.
* **Colaborador de organização:** pretende conseguir aceder e utilizar a plataforma.
* **T4J:** pretende que a plataforma permita o acesso dos colaboradores à mesma, para que possam posteriormente especificar tarefas, entre outras funções.


#### Pré-condições
n/a

#### Pós-condições
A informação do colaborador de organização é registada no sistema.

### Cenário de sucesso principal (ou fluxo básico)

1. O gestor de organização inicia a especificação do colaborador da organização.
2. O sistema solicita os dados do colaborador de organização (i.e. nome, função, contacto telefónico, endereço de email).
3. O gestor de organização introduz os dados solicitados.
4. O sistema valida e apresenta os dados ao gestor de organização, pedindo que os confirme.
5. O gestor de organização confirma.
6. O sistema **regista os dados do colaborador, tornando-o um utilizador registado** e informa o gestor de organização do sucesso da operação.



#### Extensões (ou fluxos alternativos)

*a. O gestor de organização solicita o cancelamento da especificação do colaborador de organização.

> O caso de uso termina.

4a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O gestor de organização não altera os dados. O caso de uso termina.

4b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem no sistema.
>	1. O sistema alerta o gestor de organização para o facto.
>	2. O sistema permite a sua alteração (passo 3)
>
	>	2a. O gestor de organização não altera os dados. O caso de uso termina.

4c. O sistema deteta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o gestor de organização para o facto.
> 2. O sistema permite a sua alteração (passo 3).
>
	> 2a. O gestor de organização não altera os dados. O caso de uso termina.

#### Requisitos especiais
\-

#### Lista de Variações de Tecnologias e Dados
\-

#### Frequência de Ocorrência
\-

#### Questões em aberto

* Os dados providenciados para o registo do gestor pelo utilizador não registado são os mesmos utilizados para a especificação dos colaboradores de organização?
* Quais os dados/conjuntos de dados que permitem detetar a duplicação de colaboradores de organização?
* É necessário algum mecanismo de segurança para provar que o colaborador pertence à organização do gestor que o está a especificar?
* Qual é o método utilizado para a definição da palavra-passe do colaborador? É definida pelo gestor aquando da especificação do colaborador ou é posteriormente gerada automaticamente ou segundo algum critério, sem a intervenção do gestor?
* Quais são as regras de segurança aplicáveis à palavra-passe?
* Qual é a frequência de ocorrência deste caso de uso?



## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

![UC5_MD.svg](UC5_MD.svg)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| 1. O gestor de organização inicia a especificação do colaborador de organização.   		 |	... interage com o utilizador? | EspecificarColaboradorUI   |  Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio. |
|  		 |	... coordena o UC?	| EspecificarColaboradorController | Controller    |
|  		 |	... cria instância de Colaborador| Organização   | Creator (Regra1)   |
| 2. O sistema solicita os dados necessários (i.e. nome, função, contacto telefónico, endereço de email).  		 |							 |             |                              |
| 3. O gestor de organização introduz os dados solicitados.  		 |	... guarda os dados introduzidos?  |   Colaborador | Information Expert (IE) - instância criada no passo 1     |
| 4. O sistema valida e apresenta os dados ao gestor de organização, pedindo que os confirme.   		 |	...valida os dados do Colaborador (validação local) | Colaborador |  IE. Possui os seu próprios dados. (eg: dados obrigatórios)|  	
|	 |	...valida os dados do colaborador (validação global) | Organização  | IE: A Organização possui/agrega Colaborador (eg: dados duplicados)  |
|5. O gestor de organização confirma. ||||
|6. O sistema **regista os dados do colaborador, tornando-o um utilizador registado** e informa o gestor de organização do sucesso da operação.|... guarda o Colaborador criado?| Organização |IE: No MD a Organização tem  Colaborador|
| |... regista/guarda o Utilizador referente ao Colaborador da Organizacao?|AutorizacaoFacade|IE. A gestão de utilizadores é responsabilidade do componente externo respetivo cujo ponto de interação é através da classe "AutorizacaoFacade"|
 | |... notifica o utilizador?|EspecificarColaboradorUI| |


### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Plataforma
 * Organização
 * Colaborador


Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * EspecificarColaboradorOrganizacaoUI  
 * EspecificarColaboradorOrganizacaoController


###	Diagrama de Sequência

![UC5_SD.svg](UC5_SD.svg)


###	Diagrama de Classes

![UC5_CD.svg](UC5_CD.svg)
